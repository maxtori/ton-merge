#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH







$FT account root_address --delete

$FT contract --create root_address --sign admin --deploy DuneRootSwap --params '{ "relays": [ "0x%{account:pubkey:relay1}",  "0x%{account:pubkey:relay2}",  "0x%{account:pubkey:relay3}" ], "nreqs": %{env:TON_NREQS}, "duneUserSwapCode": "%{get-code:contract:tvc:DuneUserSwap}", "merge_expiration_date": %{now:plus:%{env:SWAP_PERIOD}}, "swap_expiration_time": %{env:SWAP_EXPIRATION_TIME}, "testing": true }' || exit 2

# $FT node --give root_address:1000 || exit 2

$FT watch --account root_address &> watch-root.log &




$FT multisig -a freeton_giver --transfer ${ROOT_TONS} --to root_address || exit 2





