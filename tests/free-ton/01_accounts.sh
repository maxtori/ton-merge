#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}

export FT_SWITCH

# At this point, we expect the existence of :
# * a Surf account 'admin'
# * an `freeton_giver` account with at least 20000 TONs, that will send the
#    initial tokens to the giver_addrss and receive the remaining tokens at
#    the end of the swap
# * a 'depool' contract for vesting
# * user0...user9 contracts
# * orl contract with at least 205 TONs on it
# ./00_init.sh provides all these for sandbox

rm -f *.log *.calc *.block

$FT multisig -a freeton_giver --transfer 5 --to admin || exit 2

$FT config --deployer admin || exit 2

$FT account --create relay1 --surf --passphrase "%{env:RELAY1_PASSPHRASE}" || exit 2
# $FT node --give relay1 || exit 2

$FT account --create relay2 --surf --passphrase "%{env:RELAY2_PASSPHRASE}" || exit 2
# $FT node --give relay2 || exit 2

$FT account --create relay3 --surf --passphrase "%{env:RELAY3_PASSPHRASE}" || exit 2
# $FT node --give relay3 || exit 2
