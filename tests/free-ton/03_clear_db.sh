#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH


cmdk killall ton-merge-relay

export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ${FREETON_RELAY} --init-db
$FT client --exec --  ${FREETON_RELAY} --set-config \
    --root "%{account:address:root_address}" \
    --resend-delay $RESEND_DELAY \
    --network "%{node:url}" \
    || exit 2





export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ${FREETON_RELAY} --init-db
$FT client --exec --  ${FREETON_RELAY} --set-config \
    --root "%{account:address:root_address}"  \
    --resend-delay $RESEND_DELAY \
    --network "%{node:url}" \
     || exit 2






export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ${FREETON_RELAY} --init-db
$FT client --exec --  ${FREETON_RELAY} --set-config \
    --root "%{account:address:root_address}"  \
    --resend-delay $RESEND_DELAY \
    --network "%{node:url}" \
    || exit 2

