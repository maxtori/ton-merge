#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



cmd ${FREETON_RELAY} --get-address 1:orl_swap_addr.calc

# $FT account --create orl_swap --address $(cat orl_swap_addr.calc) --contract DuneUserSwap


# ft multisig -a freeton_giver --transfer 200 --to orl

$FT account depool --contract DePool || exit 2

$FT call depool getDePoolInfo --local || exit 2

# This should be done as part of 00_init. orl should be a participant in the
# vesting
# $FT multisig -a orl --transfer 111 --to depool addOrdinaryStake '{"stake" : "%{ton:110}" }' || exit 2

# must be at least 0.5 ton for fees
$FT multisig -a orl --transfer 1 --to depool setVestingDonor '{"donor" : "%{account:address:orl_swap}" }' || exit 2

ft call depool --local getParticipantInfo '{ "addr": "%{account:address:orl}" }'


