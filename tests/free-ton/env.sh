
NETWORK=$(cat NETWORK)

function cmd()
{
    echo
    echo  COMMAND $*
    echo
    $* || exit 2
}

function cmdk()
{
    echo
    echo  COMMAND $*
    echo
    $*
}

FT="ft --echo"
export FREETON_RELAY="../../bin/ton-merge-freeton"

export RELAY1_PASSPHRASE="still small certain catch lunar brisk tray venue shoulder judge bus auction"

export RELAY2_PASSPHRASE="gap people dumb defy all canyon treat night final kid force salt"

export RELAY3_PASSPHRASE="memory advice fever clever master weather high refuse exhaust depth range feature"

export RELAY1_DATABASE="dune_ton_merge"
export RELAY2_DATABASE="dune_ton_merge2"
export RELAY3_DATABASE="dune_ton_merge3"

export TON_MERGE_PASSPHRASE="$RELAY1_PASSPHRASE"

export TON_NREQS=2

# What the DuneRootSwap contract will start with, to be able to deploy
# DuneUserSwaps contracts with
export ROOT_TONS=19980

# What the admin Surf account will receive in the sandbox network
# > GIVER_TONS + deployment costs (5 tons) + ORL init (5 tons)
export ADMIN_TONS=20000

# export SWAP_PERIOD=7:day
export SWAP_PERIOD=120:min

# one week in seconds
# export SWAP_EXPIRATION_TIME=604800
export SWAP_EXPIRATION_TIME=3600

# how long the relay should wait before retrying a call
# export RESEND_DELAY=3600
export RESEND_DELAY=60

