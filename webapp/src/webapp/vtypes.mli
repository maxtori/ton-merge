open Ezjs_min

class type storage_js = object
  method swapStart : js_string t prop
  method swapEnd : js_string t prop
  method swapSpan : int prop
  method totalVouched : BigInt.t prop
  method vestedVouched : BigInt.t prop
  method swapped : js_string t optdef prop
  method vestedThreshold : BigInt.t prop
  method totalExchangeable : js_string t optdef prop
end

class type metal_info_js = object
  method duneAccount : js_string t optdef prop
  method duneNetwork : js_string t optdef prop
  method duneNodeUrl : js_string t optdef prop
  method balanceDun : js_string t optdef prop
  method balanceDunUnit : js_string t optdef prop
  method isAdmin : bool t prop
end

class type extraton_info_js = object
  method tonAccount : js_string t optdef prop
  method tonNetwork : js_string t optdef prop
  method tonNodeUrl : js_string t optdef prop
  method balanceTon : js_string t optdef prop
end

class type form_js = object
  method addr : js_string t prop
  method amount : js_string t prop
  method blocked : bool t prop
  method freeTONAddr : js_string t prop
  method freeTONPubkey : js_string t prop
  method secret : js_string t prop
  method dn1 : js_string t optdef prop
  method dn1Submitted : bool t prop
end

class type injected_js = object
  method text : js_string t prop
  method typ : js_string t prop
end

class type swap_js = object
  method swapId : int prop
  method duneOrigin : js_string t prop
  method dunAmount : js_string t prop
  method tonAmount : js_string t prop
  method duneStatus : js_string t prop
  method tonStatus : js_string t prop
  method hashedSecret : js_string t prop
  method time : js_string t prop
  method refundAddress : js_string t prop
  method freetonPubkey : js_string t prop
  method freetonAddress : js_string t prop
  method freetonDepool : js_string t optdef prop
  method secret : js_string t optdef prop
  method swapHash : js_string t optdef prop
  method confirmations : int prop
  method duneOp : js_string t optdef prop
  method freetonContract : js_string t optdef prop
end

class type data_js = object
  method path : js_string t prop

  (* App *)
  method kt1 : js_string t prop
  method name : js_string t prop

  (* Theme *)
  method appBg : js_string t prop
  method navbarVariant : js_string t prop
  method outline : js_string t prop
  method text : js_string t prop

  (* Metal info *)
  method metalInfo : metal_info_js t prop
  method isMetalReady : bool t prop

  (* Extraton info *)
  method extratonInfo : extraton_info_js t prop
  method isExtratonReady : bool t prop

  method storage : storage_js t optdef prop

  method form : form_js t prop

  method swaps : swap_js t js_array t prop
  method currentSwap : swap_js t optdef prop
  method advanceTimestamp : float optdef prop

  method cmd : js_string t optdef prop
  method copied : bool t optdef prop

  method injected : injected_js t optdef prop
end
