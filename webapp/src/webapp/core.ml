open Ezjs_min
open Async
open Data_types

let get_storage_theme () =
  match Optdef.to_option Dom_html.window##.localStorage with
  | None -> "light"
  | Some s ->
    begin match
        Opt.to_option (s##getItem(string "theme")) with
    | None -> "light"
    | Some js_str ->
      begin match to_string js_str with
        | "light" | "dark" as str -> str
        | _ -> "light"
      end
    end

let set_storage_theme th =
  match Optdef.to_option Dom_html.window##.localStorage with
  | None -> log "can't save theme in local storage"
  | Some s ->
    s##setItem (string "theme") th

let make_js_swapper_info si : (Vtypes.storage_js t) =
  let open Data_types in
  object%js
    val mutable swapStart =
      string @@ string_of_float @@ CalendarLib.Calendar.to_unixfloat si.swap_start
    val mutable swapEnd =
      string @@ string_of_float @@ CalendarLib.Calendar.to_unixfloat si.swap_end
    val mutable swapSpan = si.swap_span
    val mutable totalVouched = BigInt.of_string @@ Z.to_string si.total_vouched
    val mutable vestedVouched = BigInt.of_string @@ Z.to_string si.vested_vouched
    val mutable swapped = def @@ string @@ Z.to_string si.swapped
    val mutable vestedThreshold = BigInt.of_string @@ Z.to_string si.vested_threshold
    val mutable totalExchangeable = def @@ string @@ Z.to_string si.total_exchangeable
  end

let string_of_dune_status = let open Data_types in function
    | SwapSubmitted -> "submitted"
    | SwapConfirmed -> "confirmed"
    | SwapCanReveal -> "can reveal"
    | SwapCompleted -> "completed"
    | SwapExpired -> "expired"
    | SwapRefundAsked -> "refund asked"
    | SwapRefunded -> "refunded"

let string_of_ton_status = let open Data_types in function
    | SwapWaitingForConfirmation -> "waiting for confirmation"
    | SwapFullyConfirmed -> "fully confirmed"
    | SwapWaitingForCredit -> "waiting for credit"
    | SwapCreditDenied -> "credit denied"
    | SwapCredited -> "credited"
    | SwapRevealed -> "revealed"
    | SwapWaitingForDepool -> "waiting for depool"
    | SwapDepoolDenied -> "depool denied"
    | SwapTransferred -> "transferred"
    | SwapCancelled -> "cancelled"

let make_js_swap swap : (Vtypes.swap_js t) =
  let open Data_types in
  object%js
    val mutable swapId = swap.swap_id
    val mutable duneOrigin = string swap.dune_origin
    val mutable dunAmount = string @@ Z.to_string swap.dun_amount
    val mutable tonAmount = string @@ Z.to_string swap.ton_amount
    val mutable duneStatus = string @@ string_of_dune_status swap.dune_status
    val mutable tonStatus = string @@ string_of_ton_status swap.freeton_status
    val mutable hashedSecret = string @@ Bytes.to_string swap.hashed_secret
    val mutable time =
      string @@ string_of_float @@ CalendarLib.Calendar.to_unixfloat swap.time
    val mutable refundAddress = string swap.refund_address
    val mutable freetonAddress = string swap.freeton_address
    val mutable freetonPubkey = string swap.freeton_pubkey
    val mutable freetonDepool =
      match swap.freeton_depool with None -> undefined | Some s -> def @@ string s
    val mutable secret =
      match swap.secret with None -> undefined | Some s -> def @@ string @@ Bytes.to_string s
    val mutable swapHash = optdef string swap.swap_hash
    val mutable confirmations = swap.confirmations
    val mutable duneOp = undefined
    val mutable freetonContract = undefined
  end

let show_err_toaster app msg title =
  let toastr = Vue_js.get_prop app "toastr" in
  toastr##e (string msg) (string title)

let show_toaster app msg title =
  let toastr = Vue_js.get_prop app "toastr" in
  toastr##s (string msg) (string title)

let rec make_nb acc dec =
  if dec = 0 then acc else make_nb (acc ^ "0") (dec - 1)

let calcul_unit_value bal decimals =
  let bal = Z.of_string bal in
  let div = Z.of_string @@ make_nb "1" decimals in
  Z.to_string @@ Z.div bal div

let get_addr_swaps app dn1 =
  let open Data_types in
  Request.get_swaps dn1 >>= function
  | Error err ->
    log "%s" @@ snd @@ Async.error_content err ;
    ignore @@
    show_err_toaster app "Can't get swaps from api" "API error" ;
    Lwt.return_unit
  | Ok swaps ->
    app##.swaps :=
      array @@ Array.of_list @@
      List.map make_js_swap @@
      List.sort (fun sw1 sw2 -> compare sw2.swap_id sw1.swap_id) swaps ;
    Lwt.return_unit

let rec update_metal app : unit Lwt.t =
  let open Metal_api_lwt in
  let open Request in
  Lwt.catch (fun () ->
      get_account () >>= fun dn1 ->
      app##.metalInfo##.duneAccount := def @@ string dn1 ;
      get_network () >>= fun (net, node) ->
      app##.metalInfo##.duneNetwork := def @@ string net ;
      app##.metalInfo##.duneNodeUrl := def @@ string node ;
      Request.get_balance dn1 >>= function
      | Error err ->
        ignore @@
        show_err_toaster app "Can't get balance from node" "Node error" ;
        failwith @@ snd @@ error_content err
      | Ok bal ->
        app##.metalInfo##.balanceDun := def @@ string bal ;
        app##.metalInfo##.balanceDunUnit :=
          def @@ string @@ calcul_unit_value bal 6  ;
        app##.isMetalReady := _true ;
        get_addr_swaps app dn1 >>= fun () ->
        on_state_changed (fun _ -> ignore @@ update_metal app) ;
        Lwt.return_unit)
    (fun exn ->
       begin match exn with
         | Failure _ -> ()
         | _ ->
           ignore @@
           show_err_toaster app "It seems metal is not setup" "Metal connection error"
       end ;
       log "[Error in update_metal_info] %s" @@ Printexc.to_string exn ;
       app##.metalInfo##.duneAccount := undefined ;
       app##.metalInfo##.duneNetwork := undefined ;
       app##.metalInfo##.duneNodeUrl := undefined ;
       app##.metalInfo##.balanceDun := undefined ;
       app##.metalInfo##.balanceDunUnit := undefined ;
       app##.isMetalReady := _false ;
       app##.metalInfo##.isAdmin := _false ;
       app##.swaps := array [||] ;
       Lwt.return_unit)

let dune_client_deposit app =
  app##.copied := undefined ;
  let return_addr = to_string app##.form##.addr in
  let amount_form = to_string app##.form##.amount in
  let amount =
    Int64.to_string @@ Int64.of_float @@ float_of_string amount_form in
  let freeton_addr = to_string app##.form##.freeTONAddr in
  let freeton_pubkey = to_string app##.form##.freeTONPubkey in
  let secret =
    Hex.show @@
    Hex.of_bytes @@
    Bytes.of_string @@
    Digestif.SHA256.(
      to_raw_string @@
      digest_string @@
      to_string
        app##.form##.secret) in
  let si = Request.server_info () in
  let cmd =
    Printf.sprintf
      "dune-client -A %s -P %d -S transfer %s from <my_account> to %s \
       --entrypoint deposit \
       --arg '#love-json:{\"tuple\":[{\"bytes\":%S},{\"pkh\":%S},%S,%S,{\"constr\": [\"None\", null]}]}' --burn-cap 10.000000"
      si.info_node_url si.info_node_port amount si.info_kt1
      secret
      return_addr
      freeton_addr
      freeton_pubkey
  in
  app##.cmd := def @@ string cmd ;
  app##.path := string "client" ;
  Lwt.return_unit

let dune_client_swap app =
  (* SWAPID + SECRET *)
  app##.copied := undefined ;
  match Optdef.to_option app##.currentSwap with
  | None ->
    log "dune_client_swap with no swap" ;
    Lwt.return_unit
  | Some sw ->
    let id = sw##.swapId in
    let si = Request.server_info () in
    begin match Optdef.to_option sw##.secret with
      | None ->
        log "dune_client_swap with no secret" ;
        Lwt.return_unit
      | Some secret_js ->
        let secret = to_string secret_js in
        let cmd =
          Printf.sprintf
            "dune-client -A %s -P %d -S transfer 0 from <my_account> to %s \
             --entrypoint swap \
             --arg '#love-json:{\"tuple\":[%d,{\"bytes\":%S}]}' --burn-cap 10.000000"
            si.info_node_url si.info_node_port si.info_kt1 id secret in
        app##.cmd := def @@ string cmd ;
        app##.path := string "client" ;
        Lwt.return_unit
    end

let dune_client_refund app =
  app##.copied := undefined ;
  match Optdef.to_option app##.currentSwap with
  | None ->
    log "dune_client with no swap" ;
    Lwt.return_unit
  | Some sw ->
    let id = sw##.swapId in
    let si = Request.server_info () in
    let cmd =
      Printf.sprintf
        "dune-client -A %s -P %d -S transfer 0 from <my_account> to %s \
         --entrypoint refund \
         --arg '#love:%dp' --burn-cap 10.000000"
        si.info_node_url si.info_node_port si.info_kt1 id in
    app##.cmd := def @@ string cmd ;
    app##.path := string "client" ;
    Lwt.return_unit

let ton_client_reveal app =
  (* ??? *)
  app##.copied := undefined ;
  log "TODO : ton_client_reveal"

let copy app =
  match Optdef.to_option app##.cmd with
  | None -> ()
  | Some cmd ->
    Ezjs_tyxml.Clipboard.copy @@ to_string cmd ;
    app##.copied := def _true ;
    show_toaster app "" "Copied"

let extraton_reveal app =
  Lwt.catch (fun () ->
      match Optdef.to_option app##.currentSwap with
      | None ->
        log "extraton_reveal with no swap selected" ;
        Lwt.return_unit
      | Some sw ->
        let id = sw##.swapId in
        Request.get_contract_address id >>= begin function
          | Ok address ->
            let abi = _JSON##parse (string Abi.json) in
            let meth = "revealOrderSecret" in
            let obj =
              object%js
                val mutable secret =
                  string @@ Hex.show @@ Hex.of_string @@ to_string app##.form##.secret
              end in
            let args = [| Unsafe.inject obj |] in
            let w, n = Lwt.wait () in
            app##.path := string "loading" ;
            Extraton.call ~abi ~address ~meth ~args (Lwt.wakeup n) ;
            w >|= fun _ ->
            app##.path := string "swaps" ;
          | Error err ->
            ignore @@ show_err_toaster app (snd @@ Async.error_content err) "Reveal error" ;
            Lwt.return_unit
        end)
    (fun exn ->
       begin match exn with
         | Failure _ -> ()
         | _ ->
           ignore @@
           show_err_toaster app "It seems extraton is not setup" "Extraton connection error"
       end ;
       log "[Error in update_extraton_info] %s" @@ Printexc.to_string exn ;
       app##.extratonInfo##.tonAccount := undefined ;
       app##.extratonInfo##.tonNetwork := undefined ;
       app##.extratonInfo##.tonNodeUrl := undefined ;
       app##.extratonInfo##.balanceTon := undefined ;
       app##.isExtratonReady := _false ;
       Lwt.return_unit)

let refresh_swaps app =
  match Optdef.to_option app##.metalInfo##.duneAccount with
  | Some dn1 ->
    app##.path := string "loading" ;
    get_addr_swaps app (to_string dn1) >>= fun () ->
    app##.path := string "swaps" ;
    Lwt.return_unit
  | None ->
    Lwt.return_unit

let set_path app p =
  begin if p = string "swaps" then
      match
        Optdef.to_option app##.metalInfo##.duneAccount,
        Optdef.to_option app##.form##.dn1 with
      | None, None -> Lwt.return_unit
      | Some dn1, _ | None, Some dn1 ->
        app##.path := string "loading" ;
        get_addr_swaps app (to_string dn1)
    else Lwt.return_unit
  end >>= fun () ->
  app##.path := p ;
  Lwt.return_unit

let check_addr addr =
  MCrypto.check_pkh addr

let check_ton_addr addr =
  if String.get addr 0 = '0' && String.get addr 1 = ':' then
    String.length addr = 66
  else false

let check_ton_pubkey addr =
  String.length addr = 64

let reset_form app =
  app##.form##.addr := string "" ;
  app##.form##.amount := string "0" ;
  app##.form##.blocked := _false ;
  app##.form##.freeTONAddr := string "" ;
  app##.form##.freeTONPubkey := string "" ;
  app##.form##.secret := string ""

let clear_injected app =
  match Ezjs_tyxml.Manip.by_id "injected-widget" with
  | Some elt ->
    Ezjs_tyxml.Manip.removeClass elt "animate__bounceIn" ;
    Ezjs_tyxml.Manip.addClass elt "animate__bounceOut" ;
    ignore @@ Timer.set_timeout (fun () ->
        app##.injected := undefined ;
        Ezjs_tyxml.Manip.removeClass elt "animate__bounceOut" ;
        Ezjs_tyxml.Manip.addClass elt "animate__bounceIn") 1
  | None ->
    app##.injected := undefined

let relay_reveal app =
  match Optdef.to_option app##.currentSwap with
  | Some sw ->
    Lwt.catch (fun () ->
        if app##.form##.secret <> string "" then
          begin
            Request.reveal_secret
              (sw##.swapId)
              (Bytes.of_string @@ to_string app##.form##.secret)
          end >>= function
          | Ok _ ->
            show_toaster app "Secret revealed" ""
          | Error err ->
            ignore @@ show_err_toaster app (snd @@ Async.error_content err) "API call error" ;
            Lwt.return_unit
        else
          show_err_toaster app "No secret given" "")
      (fun exn ->
         ignore @@ show_err_toaster app "Something went wrong" "Failed api call" ;
         log "[Error in relay reveal] %s" @@ Printexc.to_string exn ;
         Lwt.return_unit)
  | None ->
    ignore @@ show_err_toaster app "Something went wrong" "No currentSwap to be revealed" ;
    log "[Error during swap] No currentSwap to be revealed" ;
    Lwt.return_unit

let injected_op_with_secret app op_hash op_type op_text secret =
  let open Data_types in
  let injected_js = object%js
    val mutable text = string op_text
    val mutable typ = string op_type
  end in
  let cl_to = Timer.set_timeout
      (fun () ->
         clear_injected app ;
         ignore @@ update_metal app ;
         Timer.clear_timer ()) 300 in
  Timer.create_timer 10 (fun () ->
      Lwt.async (fun () ->
          Request.get_op op_hash >>= function
          | Ok _ ->
            Timer.clear_timeout cl_to ;
            Timer.clear_timer () ;
            clear_injected app ;
            app##.path := string "loading" ;
            begin match Optdef.to_option app##.metalInfo##.duneAccount with
              | Some dn1 ->
                log "got dn1" ;
                Request.get_swaps (to_string dn1) >>= begin function
                  | Error err ->
                    log "%s" @@ snd @@ Async.error_content err ;
                    ignore @@
                    show_err_toaster app "API error" "Can't get swaps from api" ;
                    Lwt.return_unit
                  | Ok swaps ->
                    log "got swaps" ;
                    let swap =
                      List.hd @@
                      List.sort (fun sw1 sw2 -> compare sw2.swap_id sw1.swap_id) swaps in
                    app##.currentSwap := def @@ make_js_swap swap ;
                    app##.form##.secret := secret ;
                    log "before relay_reveal" ;
                    relay_reveal app >>= fun () ->
                    log "after relay_reveal" ;
                    app##.currentSwap := undefined ;
                    app##.form##.secret := string "" ;
                    log "end injected" ;
                    Lwt.return_unit
                end
              | None ->
                ignore @@
                show_err_toaster
                  app "Deposit-reveal but metal isn't connected" "Deposit Error" ;
                Lwt.return_unit
            end >>= fun () ->
            update_metal app >>= fun () ->
            app##.path := string "" ;
            Lwt.return_unit
          | Error _ -> Lwt.return_unit)
    ) ;
  app##.injected := def injected_js

let injected_op app op_hash op_type op_text =
  let injected_js = object%js
    val mutable text = string op_text
    val mutable typ = string op_type
  end in
  let cl_to = Timer.set_timeout
      (fun () ->
         clear_injected app ;
         ignore @@ update_metal app ;
         Timer.clear_timer ()) 300 in
  Timer.create_timer 10 (fun () ->
      Lwt.async (fun () ->
          Request.get_op op_hash >>= function
          | Ok _ ->
            Timer.clear_timeout cl_to ;
            Timer.clear_timer () ;
            clear_injected app ;
            update_metal app >>= fun () ->
            Lwt.return_unit
          | Error _ -> Lwt.return_unit)
    ) ;
  app##.injected := def injected_js

let deposit app =
  let open Json_utils in
  let return_addr = to_string app##.form##.addr in
  let amount_form = to_string app##.form##.amount in
  let amount =
    Int64.to_string @@
    Int64.of_float @@
    Float.trunc ((float_of_string amount_form) *. 1_000_000.) in
  let freeton_addr = to_string app##.form##.freeTONAddr in
  let freeton_pubkey = to_string app##.form##.freeTONPubkey in
  let secret =
    Bytes.of_string @@
    Digestif.SHA256.(to_raw_string @@ digest_string @@ to_string app##.form##.secret) in
  if check_addr return_addr then
    if check_ton_addr freeton_addr then
      if check_ton_pubkey freeton_pubkey then begin
        Lwt.catch (fun () ->
            let si = Request.server_info () in
            Request.deposit_entry
              ~network:si.info_network
              secret
              return_addr
              freeton_addr
              freeton_pubkey
              amount >>= function
            | Metal_api.Metal.Ok op_hash ->
              reset_form app ;
              injected_op
                app
                op_hash
                "deposit"
                "Depositing..." ;
              Lwt.return_unit
            | Metal_api.Metal.Canceled ->
              ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
              Lwt.return_unit)
          (fun exn ->
             ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
             log "[Error in send] %s" @@ Printexc.to_string exn ;
             Lwt.return_unit)
      end
      else
        begin
          ignore @@ show_err_toaster app "Not a valid FreeTON pubkey" "Form Error" ;
          Lwt.return_unit
        end
    else
      begin
        ignore @@ show_err_toaster app "Not a valid FreeTON address" "Form Error" ;
        Lwt.return_unit
      end
  else
    begin
      ignore @@ show_err_toaster app "Not a valid dune address" "Form Error" ;
      Lwt.return_unit
    end

let deposit_reveal app =
  let open Json_utils in
  let return_addr = to_string app##.form##.addr in
  let amount_form = to_string app##.form##.amount in
  let amount =
    Int64.to_string @@
    Int64.of_float @@
    Float.trunc ((float_of_string amount_form) *. 1_000_000.) in
  let freeton_addr = to_string app##.form##.freeTONAddr in
  let freeton_pubkey = to_string app##.form##.freeTONPubkey in
  let secret =
    Bytes.of_string @@
    Digestif.SHA256.(to_raw_string @@ digest_string @@ to_string app##.form##.secret) in
  let form_secret = app##.form##.secret in
  if check_addr return_addr then
    if check_ton_addr freeton_addr then
      if check_ton_pubkey freeton_pubkey then begin
        Lwt.catch (fun () ->
            let si = Request.server_info () in
            Request.deposit_entry
              ~network:si.info_network
              secret
              return_addr
              freeton_addr
              freeton_pubkey
              amount >>= function
            | Metal_api.Metal.Ok op_hash ->
              reset_form app ;
              injected_op_with_secret
                app
                op_hash
                "deposit"
                "Depositing..."
                form_secret ;
              log "after injected" ;
              Lwt.return_unit
            | Metal_api.Metal.Canceled ->
              ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
              Lwt.return_unit)
          (fun exn ->
             ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
             log "[Error in send] %s" @@ Printexc.to_string exn ;
             Lwt.return_unit)
      end
      else
        begin
          ignore @@ show_err_toaster app "Not a valid FreeTON pubkey" "Form Error" ;
          Lwt.return_unit
        end
    else
      begin
        ignore @@ show_err_toaster app "Not a valid FreeTON address" "Form Error" ;
        Lwt.return_unit
      end
  else
    begin
      ignore @@ show_err_toaster app "Not a valid dune address" "Form Error" ;
      Lwt.return_unit
    end

let refund app =
  let si = Request.server_info () in
  match Optdef.to_option app##.currentSwap with
  | Some sw ->
    Lwt.catch (fun () ->
        Request.refund_entry
          ~network:si.info_network
          (Z.of_int sw##.swapId)
        >>= function
        | Metal_api.Metal.Ok op_hash ->
          reset_form app ;
          injected_op
            app
            op_hash
            "refund"
            "Refunding..." ;
          Lwt.return_unit
        | Metal_api.Metal.Canceled ->
          ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
          Lwt.return_unit)
      (fun exn ->
         ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
         log "[Error in refund] %s" @@ Printexc.to_string exn ;
         Lwt.return_unit)
  | None ->
    ignore @@ show_err_toaster app "Something went wrong" "No currentSwap to be refunded" ;
    log "[Error during refund] No currentSwap to be refunded" ;
    Lwt.return_unit

let swap app =
  match Optdef.to_option app##.currentSwap with
  | Some sw ->
    Lwt.catch (fun () ->
        let si = Request.server_info () in
        Request.swap_entry
          ~network:si.info_network
          (Z.of_int sw##.swapId)
          (Bytes.of_string @@ to_string app##.form##.secret)
        >>= function
        | Metal_api.Metal.Ok op_hash ->
          reset_form app ;
          injected_op
            app
            op_hash
            "swap"
            "Finalizing..." ;
          Lwt.return_unit
        | Metal_api.Metal.Canceled ->
          ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
          Lwt.return_unit)
      (fun exn ->
         ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
         log "[Error in swap] %s" @@ Printexc.to_string exn ;
         Lwt.return_unit)
  | None ->
    ignore @@ show_err_toaster app "Something went wrong" "No currentSwap to be swapped" ;
    log "[Error during swap] No currentSwap to be swapped" ;
    Lwt.return_unit

let submit_dn1 app =
  let addr = match Optdef.to_option app##.form##.dn1 with
    | None -> ""
    | Some s -> to_string s in
  if check_addr addr then begin
    app##.form##.dn1Submitted := _true ;
    get_addr_swaps app addr
  end
  else
    begin
      ignore @@ show_err_toaster app "Not a valid dune address" "Form Error" ;
      Lwt.return_unit
    end

let pp_ton_amount app am =
  let num, _order, _decimal = Extraton.pp_amount @@ Int64.of_string @@ to_string am in
  string @@ num

let make_swap app sw =
  app##.currentSwap := sw ;
  set_path app @@ string "swap"

let make_reveal app sw =
  app##.currentSwap := sw ;
  set_path app @@ string "reveal"

let get_refund app sw =
  app##.currentSwap := sw ;
  set_path app @@ string "refund"

(* let mint app =
 *   let open Json_utils in
 *   let addr = to_string app##.form##.addr in
 *   let amount_form = to_string app##.form##.amount in
 *   let amount = amount_unit amount_form (get_decimals app) in
 *   log "mint %s %s" addr (Z.to_string amount) ;
 *   if check_addr addr then begin
 *     Lwt.catch (fun () ->
 *         Request.mint_entry ~network:Config.network addr amount >>= function
 *         | Metal_api.Metal.Ok op_hash ->
 *           reset_form app ;
 *           injected_op
 *             app
 *             op_hash
 *             "mint"
 *             (Printf.sprintf "Minting %s %s" amount_form (get_symbol app)) ;
 *           Lwt.return_unit
 *         | Metal_api.Metal.Canceled ->
 *           ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
 *         Lwt.return_unit)
 *       (fun exn ->
 *          ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
 *          log "[Error in mint] %s" @@ Printexc.to_string exn ;
 *          Lwt.return_unit)
 *   end
 *   else
 *     begin
 *       ignore @@ show_err_toaster app "Not a valid dune address" "Form Error" ;
 *       Lwt.return_unit
 *     end
 *
 * let burn app =
 *   let open Json_utils in
 *   let addr = to_string app##.form##.addr in
 *   let amount_form = to_string app##.form##.amount in
 *   let amount = amount_unit amount_form (get_decimals app) in
 *   log "burn %s %s" addr (Z.to_string amount) ;
 *   if check_addr addr then begin
 *     Lwt.catch (fun () ->
 *         Request.burn_entry ~network:Config.network addr amount >>= function
 *         | Metal_api.Metal.Ok op_hash ->
 *           reset_form app ;
 *           injected_op
 *             app
 *             op_hash
 *             "burn"
 *             (Printf.sprintf "Burning %s %s" amount_form (get_symbol app)) ;
 *           Lwt.return_unit
 *         | Metal_api.Metal.Canceled ->
 *           ignore @@ show_err_toaster app "Operation was canceled" "Operation not injected" ;
 *           Lwt.return_unit)
 *       (fun exn ->
 *          ignore @@ show_err_toaster app "Something went wrong" "Failed injection" ;
 *          log "[Error in burn] %s" @@ Printexc.to_string exn ;
 *          Lwt.return_unit)
 *   end
 *   else
 *     begin
 *       ignore @@ show_err_toaster app "Not a valid dune address" "Form Error" ;
 *       Lwt.return_unit
 *     end *)

let get_bg app defcl =
  let bg = to_string app##.appBg in
  let cl = [ to_string defcl ; bg ] in
  string @@ String.concat " " cl

let get_bg_text app defcl =
  let bg = to_string app##.appBg in
  let text = to_string app##.text in
  let cl = [ to_string defcl ; bg ; text ] in
  string @@ String.concat " " cl

let get_text app defcl =
  let text = to_string app##.text in
  let cl = [ to_string defcl ; text ] in
  string @@ String.concat " " cl

let get_outline app =
  app##.outline

let get_variant app =
  app##.navbarVariant

let get_table_class app defcl =
  let cl =
    if app##.appBg = string "bg-dark" then
      [ to_string defcl ; "table-dark" ]
    else [ to_string defcl ] in
  string @@ String.concat " " cl

let change_theme app theme =
  set_storage_theme theme ;
  match to_string theme with
  | "light" ->
    app##.appBg := string "bg-light" ;
    app##.navbarVariant := string "light" ;
    app##.outline := string "outline-dark" ;
    app##.text := string "text-dark"
  | "dark" ->
    app##.appBg := string "bg-dark" ;
    app##.navbarVariant := string "dark" ;
    app##.outline := string "outline-light" ;
    app##.text := string "text-light"
  | _ -> ()

let init () =
  V.add_method1 "change_theme" change_theme ;
  V.add_method1 "get_bg" get_bg ;
  V.add_method1 "get_bg_text" get_bg_text ;
  V.add_method1 "get_text" get_text ;
  V.add_method0 "get_outline" get_outline ;
  V.add_method1 "get_table_class" get_table_class ;
  V.add_method0 "get_variant" get_variant ;
  V.add_method0 "update_metal" update_metal ;
  V.add_method1 "set_path" set_path ;


  V.add_method0 "submit_dn1" submit_dn1 ;
  V.add_method1 "pp_ton_amount" pp_ton_amount ;
  V.add_method1 "make_swap" make_swap ;
  V.add_method1 "make_reveal" make_reveal ;
  V.add_method1 "get_refund" get_refund ;

  V.add_method0 "extraton_reveal" extraton_reveal ;
  V.add_method0 "relay_reveal" relay_reveal ;
  V.add_method0 "ton_client_reveal" ton_client_reveal ;

  V.add_method0 "deposit" deposit ;
  V.add_method0 "deposit_reveal" deposit_reveal ;
  V.add_method0 "dune_client_deposit" dune_client_deposit ;

  V.add_method0 "swap" swap ;
  V.add_method0 "dune_client_swap" dune_client_swap ;

  V.add_method0 "refund" refund ;
  V.add_method0 "dune_client_refund" dune_client_refund ;

  V.add_method0 "refresh_swaps" refresh_swaps ;

  V.add_method0 "copy" copy ;

  undefined

let make (app : Vtypes.data_js t) =
  Ezjs_tyxml.Clipboard.set_copy () ;
  (* TODO catch get_storage error and make a page (wrong network / kt1) *)
  let th = get_storage_theme () in
  change_theme app @@ string th ;
  Request.get_swapper_info () >>= function
  | Ok st ->
    let js_st = make_js_swapper_info st in
    app##.storage := def @@ js_st ;
    let now =
      let now = new%js date_now in
      now##valueOf /. 1000. in
    let start = parseFloat(js_st##.swapStart) in
    let total = parseFloat(js_st##.swapEnd) -. start in
    let now = now -. start in
    let adv = now /. total *. 100. in
    app##.advanceTimestamp := def adv ;
    app##.path := string "" ;
    Lwt.return_unit
  | Error err ->
    let c, str = error_content err in
    log "get_storage %d %s" c str ;
    app##.name := string "Dune-FreeTON Swapper" ;
    app##.path := string "" ;
    ignore @@ show_err_toaster app "Couldn't get contract storage" "Initialisation Error" ;
    Lwt.return_unit
