open Ezjs_min
open Vue_component

let optdef_exn f s = try f s with _ -> None
let to_int i = int_of_string (to_string i)
let to_int64 i = Int64.of_string (to_string i)
let to_z i = Z.of_string (to_string i)

let amount s_js currency =
  let s = String.lowercase_ascii @@ to_string s_js in
  let currency = to_optdef (fun s -> String.lowercase_ascii @@ to_string s) currency in
  if s = "max" || s = "maximum" then Int64.minus_one
  else match currency with
    | Some "dun" -> begin match float_of_string_opt s with
        | None -> log_str "cannot read amount"; assert false
        | Some f -> Int64.of_float @@ f *. 1_000_000. end
    | Some "udun" | Some "mudun" | None -> begin match Int64.of_string_opt s with
        | None -> log_str "cannot read amount"; assert false
        | Some i -> i end
    | Some c -> log "cannot handle currency %S" c; assert false

let pp_amount app =
  let precision = optdef_exn (to_optdef to_int) app##.precision in
  let width = optdef_exn (to_optdef to_int) app##.width in
  let order = optdef_exn (to_optdef to_int) app##.order in
  try match to_optdef to_int64 app##.amount with
    | None -> "--", -1, None, _true
    | Some amount ->
      let int_part, order, dec_part =
        Dun.pp_amount0 ?precision ?width ?order amount in
      int_part, order, dec_part, _false
  with _ ->
  match optdef_exn (to_optdef (fun x -> amount x (def (string "dun")))) app##.amount with
  | None -> "--", -1, None, _true
  | Some amount ->
    let int_part, order, dec_part =
      Dun.pp_amount0 ?precision ?width ?order amount in
    int_part, order, dec_part, _false

let pp_amount_ton app =
  let precision = optdef_exn (to_optdef to_int) app##.precision in
  let width = optdef_exn (to_optdef to_int) app##.width in
  let order = optdef_exn (to_optdef to_int) app##.order in
  try match to_optdef to_int64 app##.amount with
    | None -> "--", -1, None, _true
    | Some amount ->
      let int_part, order, dec_part =
        Extraton.pp_amount ?precision ?width ?order amount in
      int_part, order, dec_part, _false
  with _ ->
  match optdef_exn (to_optdef (fun x -> amount x (def (string "ton")))) app##.amount with
  | None -> "--", -1, None, _true
  | Some amount ->
    let int_part, order, dec_part =
      Extraton.pp_amount ?precision ?width ?order amount in
    int_part, order, dec_part, _false

let v_dun () =
  make
    ~render:Render.dun_render
    ~static_renders:Render.dun_static_renders
    ~props:(PrsArray [ "precision"; "width"; "order"; "symbol"; "amount";
                       "unit_class"; "decimal_class"; "dun_class"; "undef_title";
                       "undef_class" ])
    ~data:(fun app ->
        object%js
          val dun_cl_ = unoptdef (string "pp-dun") (Unsafe.coerce app)##.dun_class_;
          val unit_cl_ = unoptdef (string "pp-dun-unit") (Unsafe.coerce app)##.unit_class_;
          val dec_cl_ = unoptdef (string "pp-dun-decimal") (Unsafe.coerce app)##.decimal_class_;
        end)
    ~computed:(Mjs.L [
        "pp", fun app ->
          let int_part, order, dec_part, undefined_amount = pp_amount app in
          let symbol =
              (match Optdef.to_option (Unsafe.coerce app)##.symbol with
               | None -> Dun.dun_str
               | Some s -> to_string s) in
          def @@ Mjs.to_any @@ object%js
            val int = string int_part
            val dec = optdef string dec_part
            val icon = string @@ Dun.symbol_str ~order ~symbol () ;
            val undef = undefined_amount
          end])
    "v-dun"

let v_ton () =
  make
    ~render:Render.ton_render
    ~static_renders:Render.ton_static_renders
    ~props:(PrsArray [ "precision"; "width"; "order"; "symbol"; "amount";
                       "unit_class"; "decimal_class"; "ton_class"; "undef_title";
                       "undef_class" ])
    ~data:(fun app ->
        object%js
          val ton_cl_ = unoptdef (string "pp-ton") (Unsafe.coerce app)##.ton_class_;
          val unit_cl_ = unoptdef (string "pp-ton-unit") (Unsafe.coerce app)##.unit_class_;
          val dec_cl_ = unoptdef (string "pp-ton-decimal") (Unsafe.coerce app)##.decimal_class_;
        end)
    ~computed:(Mjs.L [
        "pp", fun app ->
          let int_part, order, dec_part, undefined_amount = pp_amount_ton app in
          def @@ Mjs.to_any @@ object%js
            val int = string int_part
            val dec = optdef string dec_part
            val icon = string @@ Dun.ton_symbol_str ~order () ;
            val undef = undefined_amount
          end])
    "v-ton"

let v_account () =
  make
    ~render:Render.account_render
    ~static_renders:Render.account_static_renders
    ~props:(PrsArray ["pkh"; "size"; "scale"; "blockies_class"; "hash_class"; "show_blockies";
                      "show_hash"; "len"; "height"; "net"; "ratio"])
    ~computed:(Mjs.L [
        "pr", fun app ->
          let height = unoptdef 15 app##.height in
          let ratio = unoptdef_f 1.5 to_float app##.ratio in
          let x = object%js
            val size_bl_ = unoptdef 8 app##.size
            val scale_bl_ = unoptdef 8 app##.scale
            val blockies_cl_ = unoptdef (string "blockies-img") app##.blockies_class_
            val hash_cl_ = unoptdef (string "blockies-hash") app##.hash_class_
            val sh_blockies_ = unoptdef _true app##.show_blockies_
            val sh_hash_ = unoptdef _true app##.show_hash_
            val length = unoptdef 10 app##.len
            val w_blockies_ = int_of_float (float_of_int height *. ratio)
            val w_hash_ = height
            val path = undefined
          end in
          def @@ Mjs.to_any x])
    "v-account"

let v_storage () =
  make
    ~render:Render.storage_render
    ~static_renders:Render.storage_static_renders
    ~props:(PrsArray [ "storage" ])
    "v-storage"

let v_menu () =
  make
    ~render:Render.menu_render
    ~static_renders:Render.menu_static_renders
    ~props:(PrsArray [
        "navbarVariant" ; "get_outline" ; "set_path" ; "path" ; "storage" ;
        "isMetalReady"; "update_metal"; "metalInfo" ; "change_theme" ])
    "v-menu"

let v_metal () =
  make
    ~render:Render.metal_render
    ~static_renders:Render.metal_static_renders
    ~props:(PrsArray [
        "isMetalReady" ; "update_metal" ; "metalInfo"; "change_theme" ;
        "get_outline" ; "navbarVariant" ])
    "v-metal"

let v_action () =
  make
    ~render:Render.action_render
    ~static_renders:Render.action_static_renders
    ~props:(PrsArray [
        "get_text" ; "get_bg" ; "get_bg_text" ; "get_outline" ; "get_variant" ;
        "injected" ; "path" ; "set_path" ; "form" ; "storage" ;
        "isMetalReady" ; "update_metal" ; "metalInfo"; "extraton_reveal" ;
        "isExtratonReady" ; "update_extraton" ; "extratonInfo";
        "dune_client_deposit" ; "dune_client_swap" ; "dune_client_refund" ;
        "ton_client_reveal" ; "deposit" ; "refund" ; "swap" ; "currentSwap" ;
        "relay_reveal" ; "deposit_reveal"] )
    "v-action"

let v_injected () =
  make
    ~render:Render.injected_render
    ~static_renders:Render.injected_static_renders
    ~props:(PrsArray [ "get_text" ; "get_bg" ; "injected" ] )
    "v-injected"

let v_status () =
  make
    ~render:Render.status_render
    ~static_renders:Render.status_static_renders
    ~props:(PrsArray [
        "get_text" ; "get_bg" ; "get_bg_text" ; "get_outline" ;
        "storage" ; "advanceTimestamp" ; "set_path" ] )
    "v-status"

let v_swaps () =
  make
    ~render:Render.swaps_render
    ~static_renders:Render.swaps_static_renders
    ~props:(PrsArray [
        "get_table_class" ; "get_bg" ; "get_bg_text" ; "get_outline" ;
        "get_text" ; "get_variant" ; "pp_ton_amount" ; "refresh_swaps" ;
        "isMetalReady" ; "update_metal" ; "storage" ; "submit_dn1" ; "swaps" ;
        "form" ; "get_refund" ; "make_swap" ; "make_reveal" ; "set_path" ] )
    "v-swaps"

let v_swap () =
  make
    ~render:Render.swap_render
    ~static_renders:Render.swap_static_renders
    ~props:(PrsArray [ "storage" ; "currentSwap" ] )
    "v-swap"

let v_cmd () =
  make
    ~render:Render.cmd_render
    ~static_renders:Render.cmd_static_renders
    ~props:(PrsArray [ "cmd"; "copied" ; "copy" ; "get_bg_text"; "get_text" ] )
    "v-cmd"

let load () =
  Blockies.init ();
  ignore @@ v_dun () ;
  ignore @@ v_ton () ;
  ignore @@ v_account () ;
  ignore @@ v_storage () ;
  ignore @@ v_metal () ;
  ignore @@ v_menu () ;
  ignore @@ v_action () ;
  ignore @@ v_injected () ;
  ignore @@ v_status () ;
  ignore @@ v_swaps () ;
  ignore @@ v_swap () ;
  ignore @@ v_cmd ()
