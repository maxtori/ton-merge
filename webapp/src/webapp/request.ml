(* open Sc_data *)
open Utils
open Xhr_lwt
open Lwt.Infix
(* open Metal_api_lwt *)
open Data_types

(* interface module with generated code for requests *)
module R = struct
  type url = Js_of_ocaml.Url.url

  type 'a get_request = Get of
                         url *
                         'a Json_encoding.encoding *
                         string

  let get url benc suburl = Get(url,benc,suburl)

  type ('a,'b) post_request = Post of url * 'a Json_encoding.encoding * 'b Json_encoding.encoding * string * 'a
  let output_debug = Utils.output_debug
  let post url aenc benc s v =
    Post(url,aenc,benc,s,v)
  let get_account = Metal_api_lwt.get_account
  let get_network = Metal_api_lwt.get_network
  type send_prerequest =
    Send of   (* source *) string *
              (* destination *)string *
              (* amount *)int64 *
              (* parameter *)string *
              (* entrypoint *)string
  let send ~src ~destination ~amount ~parameter ~entrypoint =
    Send (src,destination,amount,parameter,entrypoint)
  let to_string enc v = EzEncoding.construct enc v
  type originate_prerequest = Originate of string * string * int64 * string * string
  let originate ~src_lang ~src ~amount ~code ~parameter =
    Originate(src_lang,src,amount,code,parameter)
  let bigmap_getter url bm aenc benc a =
    match bm with
    | None -> Printf.eprintf "Error: empty bigmap\n"; assert false
    | Some i ->
    let suburl = Printf.sprintf "/chains/main/blocks/head/context/big_maps/%d" (Int64.to_int i) in
    Post(url,Json_utils.dune_expr aenc,Json_utils.(enc_or_null_encoding_as_option (dune_expr benc)),suburl,a)
end




let my_base_url =
  lazy (
    let url () =
      match Js_of_ocaml.Url.url_of_string
              (Js_of_ocaml.Js.to_string
                 Js_of_ocaml.Dom_html.window##.location##.href) with
      | None -> assert false
      | Some url -> url
    in
    let is_https, my_hu =
      match url () with
        Js_of_ocaml.Url.Http hu -> false, hu
      | Js_of_ocaml.Url.Https hu -> true, hu
      | _ -> assert false
    in
    let my_hu =  {
      my_hu with
      hu_path = [];
      hu_path_string = "";
      hu_arguments = [];
      hu_fragment = ""
    }
    in
    is_https,
    if is_https then
      Js_of_ocaml.Url.Https my_hu
    else
      Js_of_ocaml.Url.Http my_hu
  )

let my_base_url () =  Lazy.force my_base_url

let server_info = ref None

let node_url =
  lazy (
    match !server_info with
    | None -> assert false
    | Some si ->
      Js_of_ocaml.Url.Https {
      hu_host = si.info_node_url;
      hu_port = si.info_node_port;
      hu_path = [];
      hu_path_string = "";
      hu_arguments = [];
      hu_fragment = ""
    })
let node_url () = Lazy.force node_url

let explorer_url = lazy (
  match !server_info with
  | None -> assert false
  | Some si ->

    Js_of_ocaml.Url.Https {
      hu_host = si.info_explorer;
      hu_port = 443;
      hu_path = [];
      hu_path_string = "";
      hu_arguments = [];
      hu_fragment = ""
    } )
let explorer_url () = Lazy.force explorer_url


let api_url = lazy (
  match !server_info with
  | None -> assert false
  | Some si ->
    let hu =
      Js_of_ocaml.Url.{
      hu_host = si.info_api_url;
      hu_port = si.info_api_port;
      hu_path = [];
      hu_path_string = "";
      hu_arguments = [];
      hu_fragment = ""
    }
    in
    let is_https, _ = my_base_url () in
    if is_https then
      Js_of_ocaml.Url.Https hu
    else
      Js_of_ocaml.Url.Http hu
)
let api_url () = Lazy.force api_url


module Info = struct
  type url = R.url
  type address = Json_utils.address
  let node_url = node_url
  type contract_id = string     (* nickname *)
  let (kt1s : (contract_id,Json_utils.address) Hashtbl.t) = Hashtbl.create 10
  let get_kt1 id =
    try Hashtbl.find kt1s id with
    | Not_found ->
       (Printf.eprintf "No contract found with id %s.\
                          You may need to originate it first,\
                          or set the id %s with the set_kt1 function\n" id id; raise Not_found)
  let set_kt1 id kt1 = Hashtbl.replace kt1s id kt1
end

module Contract = Import_contract.Contract(R)(Info)

let (/) s1 s2 = s1 ^ "/" ^ s2

let dummy_encoding = (Raw ((fun s -> s),(fun s -> s)))

let enough_gas = "1000000" (* This should be enough *)

let default_error e =
  let id, msg = Async.error_content e in
  Format.eprintf "Error %i: %s@." id msg;
  Lwt.return (Error e)

let get_storage ?(_error=default_error) () =
  Utils.output_debug @@ Format.sprintf "Obtaining storage@.";
  if not(Contract.has_storage) then
    Lwt.return
      (Error
         (Async.Str_err ( "There is no public storage in this contract")))
  else
    begin
      (* Request_handling.kt1_of_origination *)
      match Info.get_kt1 "swap" with
      (* | None ->
       *    Lwt.return
       *      (Error (Async.Str_err "Error: unknown kt1 for contract")) *)
      | (* Some  *)kt1 ->
         begin
           Utils.output_debug @@ Format.sprintf "Contract %s@." kt1;
           Xhr_lwt.get
             ~base:( node_url () )
             ~args:[]
             (Enc (Json_utils.dune_expr Contract.storage_encoding))
             (
               "chains" / "main" / "blocks" / "head" / "context" / "contracts" /
                 kt1 / "storage"
             )
         end
    end

(** Metal wrappers (TODO) *)

(* bootstrap1 *)

let get_balance addr =
  Xhr_lwt.get ~base: ( node_url () )
    (Enc Json_encoding.string)
    @@ Printf.sprintf "chains/main/blocks/head/context/contracts/%s/balance" addr

let swap_entry ?network id secret =
  let open Json_utils in
  let kt1 = Info.get_kt1 "swap" in
  Metal_api_lwt.send
    ~destination:kt1
    ~entrypoint:"swap"
    ?network
    ~parameter:(R.to_string
                  (dune_expr
                     ((lovetup2 nat_encoding bytes_encoding)))
                  (id, secret))
    "0"

let refund_entry ?network id =
  let open Json_utils in
  let kt1 = Info.get_kt1 "swap" in
  Metal_api_lwt.send
    ~destination:kt1
    ~entrypoint:"refund"
    ?network
    ~parameter:(R.to_string
                  (dune_expr
                     ((lovetup1 nat_encoding)))
                  id)
    "0"

let deposit_entry ?network hsecret return_addr freeton_addr freeton_pubkey amount =
  let open Json_utils in
  let kt1 = Info.get_kt1 "swap" in
  Printf.printf "kt1 %s"kt1 ;
  Metal_api_lwt.send
    ~destination:kt1
    ~entrypoint:"deposit"
    ?network
    ~parameter:(R.to_string
                  (dune_expr
                     ((lovetup5
                         bytes_encoding
                         keyhash_encoding
                         string_encoding
                         string_encoding
                         (loveoption_encoding string_encoding))))
                     (hsecret, return_addr, freeton_addr, freeton_pubkey, None))
    amount


(* for sandbox node only, check if op is in head *)
(* let get_op op_hash =
 *   let open Json_utils in
 *   let base = Js_of_ocaml.Url.Http {
 *       hu_host = Mconfig.node_url;
 *       hu_port = Mconfig.node_port;
 *       hu_path = [];
 *       hu_path_string = "";
 *       hu_arguments = [];
 *       hu_fragment = ""
 *     } in
 *   Xhr_lwt.get
 *     ~base
 *     ~args:[]
 *     (Enc Json_utils.json)
 *     ("chains" / "main" / "blocks" / "head" / "operation" / op_hash) *)

let get_op op_hash =
  let open Json_utils in
  let base = explorer_url () in
  Xhr_lwt.get
    ~base
    ~args:[]
    (Enc Json_utils.json)
    ("v4" / "operation_nomic" / op_hash)

let get_swaps dn1 =
  let base = api_url () in
  Xhr_lwt.get
    ~base
    ~args:["origin", dn1]
    (Enc (Json_encoding.list Encoding_common.swap))
    "swaps"

let get_swapper_info () =
  let base = api_url () in
  Xhr_lwt.get
    ~base
    (Enc Encoding_common.swapper_info)
    "swapper_info"

let reveal_secret id secret =
  let base = api_url () in
  Xhr_lwt.post
    ~base
    ~content_type:"application/json"
    (Enc Encoding_common.hex_bytes)
    (Enc Json_encoding.unit)
    (Printf.sprintf "swaps/%d/reveal_secret" id)
    secret

let get_contract_address id =
  let base = api_url () in
  Xhr_lwt.get
    ~base
    (Enc Json_encoding.string)
    (Printf.sprintf "contract/%d" id)

let get_server_info () =
  let _, base = my_base_url() in
  Xhr_lwt.get
    ~base
    ( Enc Data_types.server_info_enc )
    "info.json"
  >>= function
  | Ok si ->
    server_info := Some si ;
    Lwt.return si
  | Error exn ->
    assert false

let server_info () =
  match !server_info with
  | None -> assert false
  | Some si -> si
