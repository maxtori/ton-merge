open Vtypes
open Ezjs_min

include Vue_js.Make(struct
    type data = data_js
    type all = data
    let id = "app" end)

let metal_info_empty () = object%js
  val mutable duneAccount = undefined
  val mutable duneNetwork = undefined
  val mutable duneNodeUrl = undefined
  val mutable balanceDun = undefined
  val mutable balanceDunUnit = undefined
  val mutable isAdmin = _false
end

let extraton_info_empty () = object%js
  val mutable tonAccount = undefined
  val mutable tonNetwork = undefined
  val mutable tonNodeUrl = undefined
  val mutable balanceTon = undefined
end

let form_empty () = object%js
  val mutable addr = string ""
  val mutable amount = string "0"
  val mutable blocked = _false
  val mutable freeTONAddr = string ""
  val mutable freeTONPubkey = string ""
  val mutable secret = string ""
  val mutable dn1 = undefined
  val mutable dn1Submitted = _false
end

let init ~kt1 ~storage ~render ~static_renders () =
  let data = object%js
    val mutable path = string "loading"

    val mutable kt1 = string kt1
    val mutable name = string "AppName"

    (* Theme *)
    val mutable appBg = string ""
    val mutable navbarVariant = string ""
    val mutable outline = string ""
    val mutable text = string ""

    val mutable metalInfo = metal_info_empty ()
    val mutable extratonInfo = extraton_info_empty ()

    val mutable isMetalReady = _false
    val mutable isExtratonReady = _false

    val mutable storage = storage

    val mutable form = form_empty ()

    val mutable swaps = array [||]
    val mutable currentSwap = undefined
    val mutable advanceTimestamp = undefined

    val mutable cmd = undefined
    val mutable copied = undefined

    val mutable injected = undefined

  end in
  Request.Info.set_kt1 "swap" kt1 ;
  init ~show:true ~data ~render ~static_renders ()

let () = Vc.load ()
