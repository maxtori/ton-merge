open Lwt_utils

let api_port = ref None

let set_verbose = ref false

let speclist = [
  "--port", Arg.Int (fun p -> api_port := Some p), "API server port";
  "--verbose", Arg.Int (fun i ->
      set_verbose := true;
      EzAPIServerUtils.set_verbose i), "Verbose"
]

let server services =
  Printexc.record_backtrace true;
  Arg.parse speclist (fun s -> raise (Arg.Bad ("extra argument " ^ s)))
    "API server" ;
  if not !set_verbose then EzAPIServerUtils.set_verbose 1;
  Lwt_main.run (
    let> api_port = match !api_port with
      | Some p -> Lwt.return p
      | None -> Config.get_api_port () in
    let servers = [ api_port, EzAPIServerUtils.API services ] in
    Printf.eprintf "Starting servers on ports [%s]\n%!"
      (String.concat ","
         (List.map (fun (port,_) -> string_of_int port) servers));
    EzAPIServer.server (* ~require_method:true *) servers
      ~catch:(fun _path e ->
          let code, json = Api.catch e in
          EzAPIServerUtils.Answer.return
            ~headers:EzAPIServerUtils.Answer.headers ~code
        (Ezjsonm.to_string ~minify:true json))
  )

let () =
  server Api.services
