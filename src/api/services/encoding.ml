open Json_encoding
open Encoding_common

let server_info =
  conv
    (fun o ->
       o#db_info,
       o#software_revision,
       o#software_date
    )
    (fun (
       db_info,
       software_revision,
       software_date) ->
       object
         method db_info = db_info
         method software_revision = software_revision
         method software_date = software_date
       end) @@
  obj3
    (req "db_info" version
       ~description:"Info of database")
    (req "software_revision" string
       ~description:"Revision of software for server (git commit)")
    (req "software_date" timestamp_str
       ~description:"Date of software for server")
