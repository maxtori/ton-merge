(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open EzCompat
open Lwt_utils
open Data_types

let testnet_url = "https://net.ton.dev"
let mainnet_url = "https://main.ton.dev"
let tonos_url = "http://0.0.0.0:7081"

let tvc_DuneUserSwap = match Ton_merge_contracts.read "DuneUserSwap.tvm" with
  | None -> assert false
  | Some abi -> abi

let abi_DuneUserSwap = match Ton_merge_contracts.read "DuneUserSwap.abi.json" with
  | None -> assert false
  | Some abi -> abi

let abi_DuneRootSwap = match Ton_merge_contracts.read "DuneRootSwap.abi.json" with
  | None -> assert false
  | Some abi -> abi

let keypair = match Sys.getenv "TON_MERGE_PASSPHRASE" with
  | exception Not_found -> None
  | passphrase ->
    Some ( Ton_sdk.CRYPTO.generate_keypair_from_mnemonic passphrase )

let addr_zero =
  "0:0000000000000000000000000000000000000000000000000000000000000000"



module TYPES = struct

  (* as returned by the 'getConfig' method of DuneRootSwap *)
  type getConfig_reply = {
    merge_expiration_date : string ;
    swap_expiration_time : string ;
  } [@@deriving json_encoding]

end

let json_of_string s = Hex.show (Hex.of_string s)
let string_of_json s = Hex.to_string ( `Hex s )
let json_of_swap_id swap_id = json_of_string (string_of_int swap_id)
let swap_id_of_json order_id = int_of_string ( string_of_json order_id )

let json_of_pubkey pubkey = "0x" ^ pubkey
let pubkey_of_json pubkey =
  assert ( pubkey.[0] = '0' && pubkey.[1] = 'x' );
  let len = String.length pubkey in
  String.sub pubkey 2 (len-2)

(* Keep uint256 as a string locally *)
let json_of_uint256 uint256 = "0x" ^ uint256
let uint256_of_json json =
  assert ( json.[0] = '0' && json.[1] = 'x' );
  let len = String.length json in
  String.sub json 2 (len-2)

let curtime = ref @@ Unix.gettimeofday ()
let diff_time = ref 0.
let update_curtime() =
  curtime := ( Unix.gettimeofday () -. !diff_time )
let set_time time =
  diff_time := Unix.gettimeofday () -. time

let last_ping = ref 0.

let version = 1

let maybe_ping ~client ~config ~keypair kind =
  if !last_ping +. 3600. < !curtime then begin
    last_ping := !curtime ;
    let params = Printf.sprintf {|{ "kind": %d, "version": %d }|}
        kind version in
    let> result =
      Ton_sdk.ACTION.call_lwt ~client ~server_url:config.network_url
        ~address:config.root_address ~abi:abi_DuneRootSwap
        ~meth:"relayPingPong" ~params
        ~keypair ~local:false ()
    in
    begin
      match result with
      | Ok _ ->
        Printf.eprintf "PING sent\n%!";
      | Error exn ->
        Printf.eprintf "Error in PING: %s\n%!"
          (Printexc.to_string exn)
    end;
    Lwt.return_unit
  end else
    Lwt.return_unit


let params_of_swap ?(pubkey=false) swap =
  let `Hex hashed_secret = Hex.of_bytes swap.hashed_secret in
  Printf.sprintf
    {|
{%s
  "order_id": "%s",
  "hashed_secret": "0x%s",
  "ton_amount": "%s",
  "dest": "%s",
  "depool": "%s",
  "swap_date": "%s"
}
|}
    (if pubkey then
       Printf.sprintf {| "pubkey": "0x%s",|} swap.freeton_pubkey
     else
       ""
    )
    (json_of_swap_id swap.swap_id) (* string *)
    hashed_secret  (* uint256 *)
    (Z.to_string swap.ton_amount) (* uint64 *)
    swap.freeton_address (* address *)
    (match swap.freeton_depool with
     | Some addr -> addr
     | None -> addr_zero
    ) (* address *)
    (CalendarLib.Calendar.to_unixfloat swap.time |>
     Z.of_float |> Z.to_string )

let get_swap_hash ~client ~config swap =
  let params = params_of_swap ~pubkey:true swap in
  Printf.eprintf "get_swap_hash %d\n%!" swap.swap_id;
  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url:config.network_url
      ~address:config.root_address ~abi:abi_DuneRootSwap
      ~meth:"getSwapHash" ~params
      ~local:true ()
  in
  match result with
  | Error exn ->
    Printf.eprintf "getSwapHash failed: %s\nparams: %s\n%!"
      (Printexc.to_string exn) params;
    Lwt.return_none

  | Ok json ->
    let map = Relay_misc.map_of_json json in
    let swap_hash = StringMap.find "swap_hash" map in
    match swap_hash with
    | `String swap_hash ->
      let swap_hash = uint256_of_json swap_hash in
      Lwt.return_some swap_hash
    | _ -> assert false
