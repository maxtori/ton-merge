(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types


let config_f = ref ""
let force = ref false

let set_config  ~root_address ~network_url ~resend_delay =
  let client = Ton_sdk.CLIENT.create network_url in
  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url:network_url
      ~address:root_address ~abi:Freeton.abi_DuneRootSwap
      ~meth:"getConfig" ~params:"{}" ~local:true
      ?keypair:Freeton.keypair ()
  in
  match result with
  | Error exn ->
    Printf.eprintf "set_config: exception %s\n%!" (Printexc.to_string exn);
    exit 2
  | Ok result ->
    let {
      Freeton.TYPES.merge_expiration_date ;
      swap_expiration_time ;
    } = EzEncoding.destruct Freeton.TYPES.getConfig_reply_enc result in
    let merge_expiration_date =
      Int64.of_string merge_expiration_date in
    let config = {
      Data_types.network_url ;
      root_address ;
      merge_expiration_date ;
      swap_expiration_time = Int64.of_string swap_expiration_time ;
      relay_pubkey = ( match Freeton.keypair with
          | None ->
            Printf.eprintf "TON_MERGE_PASSPHRASE must be defined\n%!";
            exit 2
          | Some key_pair -> key_pair.public);
      resend_delay ;
    } in
    let> () = Db.CONFIG.set config in
    Lwt.return config

let get_config () =
  let> config = Db.CONFIG.get () in
  match config with
  | Some config ->
    Printf.eprintf "Current config: \n%!";
    Printf.eprintf "   network_url  : %s\n%!" config.network_url ;
    Printf.eprintf "   relay_pubkey : %s\n%!" config.relay_pubkey ;
    Printf.eprintf "   root_address : %s\n%!" config.root_address ;
    Printf.eprintf "   merge_expiration_date : %Ld\n%!"
      config.merge_expiration_date ;
    Printf.eprintf "   merge_expiration_time : %Ld\n%!"
      ( Int64.sub config.merge_expiration_date
          (Int64.of_float @@ Unix.gettimeofday () ) );
    Printf.eprintf "   swap_expiration_time: %Ld\n%!"
      config.swap_expiration_time;
    Printf.eprintf "   resend_delay : %.0f\n%!" config.resend_delay ;
    Lwt.return config
  | None ->
    Printf.eprintf "No config. You must call with --root ROOT.\n%!";
    exit 2

let start_daemon () =
  let monitor_freeton = ref None in
  let monitor_database = ref None in

  Sys.set_signal Sys.sigint (Sys.Signal_handle (fun signal ->
      begin
        match !monitor_database with
        | None -> () | Some pid ->
          (try Unix.kill pid signal with _ -> ());
      end;
      begin
        match !monitor_freeton with
        | None -> () | Some pid ->
          (try Unix.kill pid signal with _ -> ());
      end;
      exit 2
    ));


  let string_of_status = function
    | Unix.WEXITED n -> Printf.sprintf "exited %d" n
    | Unix.WSIGNALED n -> Printf.sprintf "signaled %d" n
    | Unix.WSTOPPED n -> Printf.sprintf "stopped %d" n
  in
  let rec iter config =

    let f ~name ~arg ~ref =
      let restart () =
        let log = Printf.sprintf "%s-%s.log"
            (Project_config.database()) name in
        let oc = open_out_gen
            [ Open_wronly ; Open_append ; Open_creat ] 0o644
            log in
        let fd = Unix.descr_of_out_channel oc in
        Printf.fprintf oc "Starting relay %s \n\n%!" name;
        Printf.eprintf "Starting relay %s \n\n%!" name;
        let pid = Unix.create_process
            Sys.argv.(0)
            [| Sys.argv.(0) ; arg |]
            Unix.stdin fd fd
        in
        close_out oc;
        ref := Some pid;
        Lwt.return_unit
      in
      let> old_pid = Relay_misc.get_pid name in
      match old_pid, !ref with
      | None, Some _old_pid ->
        Printf.eprintf "Process %s died. Restarting\n%!" name;
        restart ()
      | None, None ->
        Printf.eprintf "Starting process %s\n%!" name;
        restart ()
      | Some pid, None ->
        Printf.eprintf
          "Process %s under pid %d is already running\n%!" name pid;
        Lwt.return_unit
      | Some _, Some _ ->
        Lwt.return_unit
    in

    let> () = f ~name:"monitor_database" ~arg:"--monitor-database"
        ~ref:monitor_database
    in
    let> () = f ~name:"monitor_freeton" ~arg:"--monitor-freeton"
        ~ref:monitor_freeton in

    begin
      match !monitor_freeton, !monitor_database with
      | None, None ->
        Printf.eprintf "Fatal error: both monitors are already managed by another process\n%!";
        exit 2
      | _ -> ()
    end;
    let> res = Lwt_unix.wait () in
    match res with
    | exception exn ->
      Printf.eprintf "Exception: %s\n%!" (Printexc.to_string exn);
      exit 2
    | ( pid, status ) ->
      Printf.eprintf "monitor with pid %d died with status %s\n%!" pid
        (string_of_status status);
      let> () = Lwt_unix.sleep 5. in
      iter config
  in
  Lwt_main.run (
      let> config = get_config () in
      iter config )


let set_config ?root_address ?network_url ~resend_delay () =
  match root_address with
  | None ->
    Printf.eprintf "You must always specify  --root ROOT\n%!";
    exit 2
  | Some root_address ->
    set_config ~root_address
      ~network_url:(match network_url with
          | None -> Freeton.tonos_url
          | Some url -> url)
      ~resend_delay:(match resend_delay with
          | None -> 3600.
          | Some resend_delay -> resend_delay)

(* These are tests using dune-ix and ft configs for sandboxing.
   swap_100 is a swap of dune-ix user0 to fr user0 of 10 DUN *)
let test_swaps =
  let secret = "Bonjour" in
  let hashed_secret =
    Hex.to_bytes
      (`Hex
         "9172e8eec99f144f72eca9a568759580edadb2cfd154857f07e657569493bc44")
  in
  let swap =     {
      swap_id = 0;
      dune_origin = "";
      dun_amount = Z.of_string "90_000_000";
      ton_amount = Z.zero; (* not used *)
      dune_status = SwapSubmitted ;
      freeton_status = SwapWaitingForConfirmation;
      hashed_secret ;
      time = Encoding_common.subst_date_time ; (* not used *)
      refund_address =  "";
      freeton_pubkey = "";
      freeton_address = "";
      freeton_depool = None;
      secret = Some (Bytes.of_string secret) ;
      swap_hash = None ;
      confirmations = 0 ;
      logical_time = 0L ;
    }
  in
  List.mapi (fun i dune_origin ->
      { swap with
        swap_id = 100 + i;
        dune_origin ;
        dun_amount = Z.of_string (Printf.sprintf "%d_000_000_000" ((i+1)*10));
        freeton_pubkey = Printf.sprintf "%%{account:pubkey:user%d}" i;
        freeton_address = Printf.sprintf "%%{account:address:user%d}" i;
      })
    [
      "dn1QudFDa1k15xwnXWYxgMRJf4RGJV1yzDym" ;
      "dn1YJFYFw6ER8GFct3JA6ktRT1pjynV1juCy" ;
      "dn1KhdkBRSRdPUVtXZaz9kNMX3nxd92LVBwH" ;
      "dn1HEggbbpWYovvKzpnBfpSN82fip56fk5K2" ;
      "dn1ZFsha5jeedydCiUxj3rLNKoKN7dmymdNg" ;
      "dn1NL8VZb7GeWCV8RYA6b3JnEDnh9B7Z2XjT" ;
      "dn1Gc5pFRGzBsnTifsNogmhiQcPHvcehbX4s" ;
      "dn1SVEoV68ofifURFHeXHVRuDHETmv3HedRa" ;
      "dn1d8CXw3GEXKcZvBYzNFWgv4jDEojRXArZo" ;
      "dn1aMH2u9D53RM74UeosvYh2c3L2k49H7iQa"
    ]

let fake config swaps =
  match swaps with
  | [] -> Lwt.return_unit
  | _ ->
    let client = Ton_sdk.CLIENT.create config.network_url in
    Lwt_list.iter_s (fun file ->
        let s = EzFile.read_file file in
        Printf.eprintf "s = %s\n%!" s;
        let s = EzEncoding.destruct Encoding_common.swap s in
        let ton_amount = Data_types.ton_of_dun s.dun_amount in
        let refund_address = if s.refund_address = "" then
            s.dune_origin else s.refund_address in
        Printf.eprintf "Add swap in DB\n%!";

        begin match s.secret with
          | None -> ()
          | Some secret ->
            assert ( Db.check_secret s secret )
        end;

        let> res = Db.SWAPS.get ~swap_id: s.swap_id in
        match res with
        | None ->
          let swap =  { s with dune_status = SwapConfirmed ;
                               ton_amount ; refund_address } in
          let> res = Freeton.get_swap_hash ~client ~config swap in
          begin
            match res with
            | None ->
              Printf.eprintf "Could not retrieve swap hash\n%!";
              exit 2
            | Some swap_hash ->
              let> () = Db.SWAPS.add swap in
              let oc = open_out (file ^ ".hash") in
              Printf.fprintf oc "%s\n%!" swap_hash;
              close_out oc;
              Lwt.return_unit
          end
        | Some _ ->
          Printf.eprintf "Swap %d is already in database\n%!" s.swap_id;
          Lwt.return_unit
      ) swaps


type action =
  | No_action
  | Daemon
  | Close_swap
  | Monitor_database
  | Monitor_freeton
  | Set_secret of int * string
  | Get_address of int * string
  | Set_config

let () =
  let root_address = ref None in
  let network_url = ref None in
  let resend_delay = ref None in
  let action = ref No_action in
  let dune_swaps = ref [] in

  Arg.parse [

    "--init-db", Arg.Unit (fun () ->
        let dbh = PGOCaml.connect ~database:(Project_config.database()) () in
        EzPG.upgrade_database ~upgrades:Db_common.VERSIONS.(!upgrades) dbh;
        Printf.eprintf "Upgrade done\n%!";
        exit 0
      ),
    " Init database";

    "--root", Arg.String (fun s -> root_address := Some s),
    "ROOT Init with root address";
    "--network", Arg.String (fun s ->
        network_url := Some (match s with
            | "main" | "mainnet" -> Freeton.mainnet_url
            | "test" | "testnet" -> Freeton.testnet_url
            | _ -> s)),
    "NETWORK_URL Url of freeton node";

    "--resend-delay", Arg.Float (fun f -> resend_delay := Some f),
    "NSECONDS Number of seconds to wait before resend (3600. for mainnet)";

    "--save-test-swaps", Arg.Unit (fun () ->
        List.iteri (fun i s ->
            let s = EzEncoding.construct Encoding_common.swap s in
            EzFile.write_file
              (Printf.sprintf "test-swap-%d.json.in" (100 + i)) s
          ) test_swaps ;
        Printf.eprintf "Test swaps saved to files.\n%!";
        exit 0
      ),
    " Save test swaps to files";

    "--test-swap", Arg.String (fun s ->
        dune_swaps := s :: !dune_swaps
      ),
    "SWAP.json Load a swap from a file (testing)";

    "--set-secret", Arg.String (fun s ->
        let swap_id, secret = EzString.cut_at s ':' in
        let swap_id = int_of_string swap_id in
        action := Set_secret ( swap_id, secret );
      ), "SWAP_ID:SECRET Set secret for swap id";

    "--get-address", Arg.String (fun s ->
        let swap_id, file = EzString.cut_at s ':' in
        let swap_id = int_of_string swap_id in
        action := Get_address ( swap_id, file );
      ), "SWAP_ID:FILE Lookup SWAP_ID in DB and store contract's address in FILE";

    "--daemon", Arg.Unit (fun () -> action := Daemon ),
    "Start the relay (two processes in the background)";

    "--monitor-database", Arg.Unit (fun () -> action := Monitor_database ),
    "Start the relay to monitor the database";

    "--monitor-freeton", Arg.Unit (fun () -> action := Monitor_freeton ),
    "Start the relay to monitor freeton";

    "--set-config", Arg.Unit (fun () -> action := Set_config ),
    "Set the initial configuration";

    "--close-swap", Arg.Unit (fun () -> action := Close_swap ),
    "Close swap" ;
  ] (fun s -> config_f := s)
    "Dune TON swapper deployer\nUsage:\n\tton-merge-swap-deploy <config.json>";
  try
    match !action with

    | Daemon -> start_daemon ()

    | Monitor_database ->
      let name = "monitor_database" in
      Lwt_main.run (
        let> config = get_config () in
        let> not_running = Relay_misc.check_pid name in
        if not_running then
          let> () = Relay_misc.register_pid name in
          Monitor_database.monitor ?resend_delay:!resend_delay config
        else
          Lwt.return_unit
      )

    | Monitor_freeton ->
      let name = "monitor_freeton" in
      Lwt_main.run (
        let> config = get_config () in
        let> not_running = Relay_misc.check_pid name in
        if not_running then
          let> () = Relay_misc.register_pid name in
          Monitor_freeton.monitor config
        else
          Lwt.return_unit
      )

    | Set_secret (swap_id, secret) ->
      Lwt_main.run (
        let> ok = Db.SWAPS.set_secret ~swap_id (Bytes.of_string secret) in
        if ok then
          Printf.eprintf "Secret entered in DB\n%!";
        Lwt.return_unit
      )

    | Get_address (swap_id, file) ->
      Lwt_main.run (
        let rec iter () =
          let> swap = Db.SWAPS.get ~swap_id in
          match swap with
          | None ->
            Printf.eprintf "No swap %d\n%!" swap_id;
            exit 2
          | Some swap ->
            match swap.swap_hash with
            | None ->
              let> () = Lwt_unix.sleep 1.0 in
              iter ()
            | Some swap_hash ->
              let> res = Db.CONTRACTS.get ~swap_hash in
              match res with
              | None ->
                let> () = Lwt_unix.sleep 1.0 in
                iter ()
              | Some address ->
                EzFile.write_file file address;
                Lwt.return_unit
        in
        iter ()
      )

    | Close_swap ->
      Lwt_main.run (
        let> config = get_config () in
        Close_swap.close_swap config )

    | Set_config ->
      Lwt_main.run (
        let> _config = set_config ?root_address:!root_address
            ?network_url:!network_url ~resend_delay:!resend_delay () in
        Printf.eprintf "Config set. Exiting.\n%!";
        Lwt.return_unit
      )

    | No_action ->
      if !root_address == None && !network_url == None then
        Lwt_main.run (
          let> config = get_config () in
          let> () = fake config !dune_swaps in
          Printf.eprintf "Nothing to do. Exiting.\n%!";
          Lwt.return_unit
        )
      else
        begin
          Printf.eprintf "Use --set-config to set the initial config.\n%!";
          exit 2
        end

  with
    Failure s ->
    Format.eprintf "Error: %s@." s;
    exit 2
