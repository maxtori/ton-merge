open Lwt_utils
open Dune.Types

(* let init_db () =
 *   let dbh = PGOCaml.connect ~database:Project_config.database () in
 *   EzPG.upgrade_database ~upgrades:Versions.(!upgrades) dbh *)

open Db_common

let analyze_exn exn : unit =
  let print_unknown fmt = function
    | EzEncoding.DestructError ->
      Format.fprintf fmt "Error while destructing json@."
    | Failure e ->
      Format.fprintf fmt "Unexpected error: %s@." e
    | _ ->
      Format.fprintf fmt "Unexpected error: %s@." (Printexc.to_string exn)
  in
  Format.eprintf "@[<hov>%a@]@."
    (Json_encoding.print_error ~print_unknown) exn

(* let print_res_error e =
 *   (match e.Error.error with
 *   | Error.Exception exn -> analyze_exn exn
 *   | _ -> ()
 *   );
 *   Format.eprintf "%s" (Error.to_string e) *)

let catch_exn f =
  Lwt.catch
    (fun () -> let> r = f () in match r with
       | Ok () -> return_u
       | Error e ->
         Format.eprintf "Crawler error : @[%s@].@." e;
         (* print_res_error e; *)
         return_u)
    (fun exn ->
       Format.eprintf "Crawler unhandled exception.@.";
       analyze_exn exn; return_u)

let reset_chain dbh cross_hash =
  let rec aux hash =
    if hash <> cross_hash then (
      let@ () = Crawler_blocks.set_main dbh ~main:false ~block_hash:hash in
      let@ r = Db.block_predecessor dbh hash in
      match r with
      | [ Some pred, _ ] -> aux pred
      | _ -> return (Ok ()))
    else return (Ok ()) in
  let> hash = Db.head_hash dbh >|= function None -> failwith "No head" | Some h -> h in
  aux hash

let update_chain hash pred_hash =
  transaction @@ fun dbh ->
  let rec aux hash pred_hash =
    let@ r = Db.block_predecessor dbh pred_hash in
    match r with
    | [ _, true ] ->
      let@ () = reset_chain dbh pred_hash in
      Crawler_blocks.set_main dbh ~block_hash:hash
    | [ Some pred, false ] ->
      let@ () = aux pred_hash pred in
      Crawler_blocks.set_main dbh ~block_hash:hash
    | [ None, false ] | [] ->
      Crawler_blocks.set_main dbh ~block_hash:hash
    | _ ->
      Format.ksprintf Lwt.return_error (* .(error CrawlerError) *)
        "Can't recover main chain status for %s" pred_hash in
  Format.printf "update chain  %s -> %s@."
    (String.sub pred_hash 0 10)
    (String.sub hash 0 10);
  let@! () = aux hash pred_hash in
  Format.printf "update chain ok@."

let register_forward ~start ~until =
  let rec aux level =
    Format.printf "request       %d@." level;
    let@ block = Crawler_request.block ~first:(level<2) (string_of_int level) in
    let operations = List.flatten block.node_operations in
    let@ known_in_main = Crawler_blocks.register_block ~operations block in
    let@ () =
      if known_in_main then return_ok ()
      else if level > start then
        update_chain
          block.node_hash block.node_header.header_shell.shell_predecessor
      else
        Crawler_blocks.set_main_dbh block.node_hash
    in
    if level < until then aux (level+1)
    else return_ok ()
  in
  if start > until then return_ok ()
  else (
    Format.printf "Crawl forward from %d to %d@." start until;
    aux start
  )

let counter = ref 1

let rec register hash =
  let shash = String.sub hash 0 10 in
  let@ shell = Crawler_request.shell hash in
  let predecessor_hash = shell.shell_predecessor in
  let level = shell.shell_level in
  let@ registered, _ = Db.block_registered predecessor_hash in
  let> first_level = Config.get_first_level () in
  let go_back = match first_level with
    | None -> not registered
    | Some start -> not registered && level > start in
  let@ () =
    if go_back then (
      Format.printf "%d: Missing predecessor %s@." level predecessor_hash;
      register predecessor_hash
    )
    else return (Ok ())
  in
  let@ registered, known_in_main = Db.block_registered hash in
  let@ () =
    if not registered then (
      counter := 1;
      Format.printf "\nrequest       %s at level %d\n%!" shash level;
      let@ block = Crawler_request.block hash in
      let operations = List.flatten block.node_operations in
      let@! _ = Crawler_blocks.register_block ~operations block in
      ()
    )
    else (
      incr counter;
      let> verbose = Config.get_verbose () in
      Format.printf "%shead          %s (x%d)%!%s"
        (if verbose > 0 then "" else if !counter = 2 then "\n" else "\r")
        shash !counter (if verbose > 0 then "\n" else "");
      return_ok ()) in
  if not known_in_main then update_chain hash predecessor_hash
  else return_ok ()

let rec loop () =
  let> () = catch_exn (fun () ->
      let@ head_hash = Crawler_request.head_hash () in
      register head_hash) in
  let> crawler_sleep = Config.get_crawler_sleep () in
  let> () = Lwt_unix.sleep crawler_sleep in
  loop ()

let init () =
  (* init_db (); *)
  catch_exn @@ fun () ->
  let> confirmations_needed = Config.get_confirmations () in
  let> forward = Config.get_forward () in
  let> first_level = Config.get_first_level () in
  let@ cl = Crawler_request.current_level () in
  Format.printf "Blockchain current level %d@." cl.node_lvl_level;
  let cl = cl.node_lvl_level - (2 * Int32.to_int confirmations_needed) in
  let> ll = Db.last_level () in
  Format.printf "Last registered level %d@." ll;
  let until = match forward with
    | Some level -> min level cl
    | None -> max cl 1 in
  let start = match first_level with
    | Some level -> max (min level until) ll
    | None -> min until ll in
  Format.printf "Registering forward from %d to %d@." start until;
  register_forward ~start ~until
