(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 Origin Labs                                          *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils

type action =
    Deploy of string
  | Setup of string

let run config force action =

  let> () =
    match config with

    | None ->
      let> c = Config.read_from_db () in
      print_endline ( Config.to_string c );
      Lwt.return_unit

    | Some file ->
      let c = Config.read_from_file file in
      Config.write_to_db c

  in
  match action with
  | None -> Lwt.return_unit
  | Some ( Deploy file ) -> Deploy.deploy ~force file
  | Some ( Setup file ) -> Deploy.setup file

let () =
  let config = ref None in
  let force = ref false in
  let action = ref None in
  Arg.parse [

    "--init-db", Arg.Unit (fun () ->
        let database = Project_config.database() in
        let dbh = match PGOCaml.connect ~database () with
          | exception _exn ->
            EzPG.createdb database ;
            PGOCaml.connect ~database ()
          | dbh -> dbh
        in
        EzPG.upgrade_database ~upgrades:Db_common.VERSIONS.(!upgrades) dbh;
        Printf.eprintf "Upgrade done\n%!";
      ),
    " Init database";

    "--force", Arg.Set force, "Force redeploy even if contract already known" ;
    "--config", Arg.String (fun s -> config := Some s),
    "FILENAME Enter configuration" ;
    "--deploy", Arg.String (fun s -> action := Some ( Deploy s )),
    "FILENAME Deploy contract" ;
    "--setup", Arg.String (fun s -> action := Some ( Setup s )),
    "FILENAME Setup Dune relay config";
  ] (fun s ->
      Printf.eprintf "Error: unexecpted option %S\n%!" s;
      exit 2 )
    "Dune TON swapper init\nUsage:\n\tton-merge-init";
  try
    Lwt_main.run @@
    run !config !force !action
  with
    Failure s ->
    Format.eprintf "Error: %s@." s;
    exit 2
