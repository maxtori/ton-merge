(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 Origin Labs                                          *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types
open Encoding_common
open Blockchain_common


let timestamp_enc = timestamp
let swapper_info_enc = swapper_info
module Z = struct
  include Z
  let t_enc = amount
end

type contract = {
  deployer_alias : string;
  delegate: string option;
  admin: string;
  swap_start: timestamp;
  swap_end: timestamp;
  swap_span: int;
  vested_threshold: Z.t;
  total_exchangeable: Z.t;
}  [@@deriving json_encoding]

type setup = {
  addr : string ;
  level : int ;
  info : swapper_info ;
  big_map : int ;
}  [@@deriving json_encoding]

let read_json enc file =
  try
    let s = EzFile.read_file file in
    EzEncoding.destruct enc s
  with exn ->
    Format.eprintf "Could not read or parse file %s: @[<hv>%a@]@."
      file (Json_encoding.print_error ?print_unknown:None) exn;
    raise exn

module DEPLOY : sig

  val run : string -> setup Lwt.t

end = struct

  let read_config_from_file file = read_json contract_enc file

  let aps (EzAPI.TYPES.BASE node) =
    let uri = Uri.of_string node in
    let tls = match Uri.scheme uri with
      | Some "https" -> true
      | Some "http" | None -> false
      | Some s -> failwith ("unuspported node scheme "^ s) in
    let port = match Uri.port uri with
      | Some p -> p
      | None when tls -> 443
      | None -> 80 in
    let host = match Uri.host uri with
      | None -> "localhost"
      | Some s -> s in
    host, port, tls

  let run file =
          let contract = read_config_from_file file in
      let> node = Config.get_nodes () >|= List.hd in
      let host, port, tls = aps node in
      let param_json =
        Json_encoding.construct
          Swap_contract.__init_storage_parameter_enc
          (contract.admin,
           contract.swap_start |> timestamp_to_zstr,
           contract.swap_end |> timestamp_to_zstr,
           contract.swap_span,
           contract.vested_threshold,
           contract.total_exchangeable) in
      let param_json_s = Ezjsonm.value_to_string param_json in
      let name_contract = Printf.sprintf "swap_%ld" (Random.int32 Int32.max_int) in
      let code_json = Ezjsonm.value_from_string Swap_code.mycode in
      let code_json = match code_json with
        | `O [ "dune_code", code ] -> code
        | _ -> code_json in
      let code_file, oc = Filename.open_temp_file name_contract ".json" in
      output_string oc @@
      "#love-json\n" ^
      Ezjsonm.value_to_string code_json;
      close_out oc;
      let cmd = Format.sprintf
          "dune-client --wait none -A %s -P %d %s \
           originate contract %s \
           transferring 0 from %s \
           running %s \
           --init '#love-json:%s' \
           %s \
           --burn-cap 20 --force --no-locs -q"
          host
          port
          (if tls then "-S" else "")
          name_contract
          contract.deployer_alias
          code_file
          param_json_s
          (match contract.delegate with
           | None -> ""
           | Some d -> "--delegate " ^ d)
      in
      Format.eprintf "Deploy command: \n%s\n@." cmd;
      let res = Sys.command cmd in
      if res <> 0 then
        Format.ksprintf failwith "Error %d on deploy" res;
      let kt1_file = Filename.temp_file name_contract ".kt1" in
      let cmd =
        Format.sprintf
          "dune-client show known contract %s 2> /dev/null | tr -d '[:space:]' > %s"
          name_contract kt1_file in
      let res = Sys.command cmd in
      if res <> 0 then
        Format.ksprintf failwith "Error %d: unknown contract %s" res name_contract;
      let ic = open_in kt1_file in
      let addr = input_line ic in
      close_in ic;
      Format.printf "Swap contract is %s, saving to database ...@." addr;
      let EzAPI.TYPES.BASE n = node in
      let> level, block =
        EzCurl_lwt.get (EzAPI.TYPES.URL (n ^ "chains/main/blocks/head/header")) >|= function
        | Error (code, _) ->
          Format.ksprintf failwith "Cannot get head level, http code %d" code
        | Ok j ->
          let j = Ezjsonm.value_from_string j in
          let level =
            Ezjsonm.find j ["level"] |> Ezjsonm.get_int
          in
          let block =
            Ezjsonm.find j ["hash"] |> Ezjsonm.get_string
          in
          (level, block)
      in
      Format.printf "First level is %d@." level;
      let rec get_storage retry =
        if retry = 0 then failwith "Could not read storage for contract";
        Format.printf "Sleeping for %d s before retrieving storage...@." 5;
        let> () = Lwt_unix.sleep 5. in
        Swap_contract.get_storage ?block:None addr >>= function
        | Error exn ->
          Printf.eprintf "Error(%d): %s\n%!" retry
            (EzRequest_lwt.string_of_error (fun _ -> assert false) exn);
          get_storage (retry - 1)
        | Ok x -> Lwt.return x
      in
      let> storage = get_storage 30 in
      let info = swap_storage_to_swapper_info storage in
      let big_map = match storage.swaps with
        | None -> assert false
        | Some bmid -> Z.to_int bmid in
      Lwt.return { addr ; level ; info ; big_map }


end

let () = Random.self_init ()


let setup_db { addr ; level ; info ; big_map } =
  Format.printf "Saving contract parameters to database ...@.";
  let> () = Db.register_swap_contract_address ~addr in
  let> () = Config.set_first_level level in
  let> () = Db.set_swapper_info info in
  let>! () = Db.set_swaps_big_map big_map in
  Format.printf "\nDONE.@."

let deploy ~force file =
  let> () =
    Db.get_swap_contract_address () >|= function
    | Some s when not force ->
      Format.ksprintf failwith
        "Swap contract already deployed at %s (use --force to redeploy)" s
    | _ -> ()
  in
  let> setup = DEPLOY.run file in
  let s = EzEncoding.construct setup_enc setup in
  EzFile.write_file "setup.json" s ;
  Printf.eprintf "File %S generated\n%!" "setup.json";
  setup_db setup


let setup file =
  let setup = read_json setup_enc file in
  setup_db setup
