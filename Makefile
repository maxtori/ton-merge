LIQUIDITY:=liquidity
DATABASE:=dune_ton_merge
HAS_FT:=false
HAS_SPICE:=false
HAS_LIQUIDITY:=false
HAS_JSOO:=false

-include Makefile.config

DUNE_CONTRACTS_DIR:=contracts/dune-network
DUNE_TZ_CONTRACTS_DIR:=contracts/dune-network/michelson
DUNE_LOVE_CONTRACTS_DIR:=contracts/dune-network/love
LIQ_CONTRACTS = $(wildcard $(DUNE_CONTRACTS_DIR)/*.liq)
TZ_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(DUNE_TZ_CONTRACTS_DIR)/%.tz, $(LIQ_CONTRACTS))
LOVE_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json, $(LIQ_CONTRACTS))
CONTRACTS_DEST:=src/dune-network/contracts
SPICED_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(CONTRACTS_DEST)/contract_%.ml, $(LIQ_CONTRACTS))
SPICED_CODES = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(CONTRACTS_DEST)/%_code.ml, $(LIQ_CONTRACTS))

DBVERSION=$(shell psql $(DATABASE) -c "select value from ezpg_info where name='version'" -t -A)
DBCONVERTERS=src/db/db/custom_converters

all: dune-contracts build crawler deployer server openapi ton-merge-freeton freeton-contracts injector webapp

build: db-update spiced-contracts freeton-contracts webapp-dunes submodule
	@echo "Building ..."
	PGDATABASE=$(DATABASE) PGCUSTOM_CONVERTERS_CONFIG=$(DBCONVERTERS) dune build --profile release
	@echo "OK"

webapp-dunes:
ifeq (${HAS_JSOO},false)
	@echo HAS_JSOO=false
	rm -f webapp/src/webapp/template/dune
	rm -f webapp/src/webapp/dune
else
	ln -sf dune_ webapp/src/webapp/template/dune
	ln -sf dune_ webapp/src/webapp/dune
endif

ton-merge-freeton: _build/default/src/free-ton/relay/main.exe
	@mkdir -p bin
	@rm -f bin/freeton-relay
	@cp -fp _build/default/src/free-ton/relay/main.exe bin/ton-merge-freeton

freeton-contracts:
ifeq (${HAS_FT},false)
	echo HAS_FT=false
else
	( cd contracts/free-ton ; $(MAKE) )
endif

crawler: _build/default/src/dune-network/crawler/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/dune-network/crawler/main.exe bin/ton-merge-dune-crawler

#config-init: _build/default/src/dune-network/config/init/main.exe
#	@mkdir -p bin
#	@cp -fp _build/default/src/dune-network/config/init/main.exe bin/ton-merge-config-init

deployer: _build/default/src/dune-network/deploy/main.exe
	@rm -f bin/ton-merge-config-init bin/ton-merge-deploy
	@mkdir -p bin
	@cp -fp _build/default/src/dune-network/deploy/main.exe bin/ton-merge-init

server: _build/default/src/api/server/api_server.exe
	@mkdir -p bin
	@rm -f bin/ton-merge-server
	@cp -fp _build/default/src/api/server/api_server.exe bin/ton-merge-api-server

injector: _build/default/src/dune-network/injector/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/dune-network/injector/main.exe bin/ton-merge-dune-injector

ifeq (${HAS_JSOO},false)
webapp:
	@echo HAS_JSOO=false
else
webapp: _build/default/webapp/src/webapp/main.bc.js
	cp -fp _build/default/webapp/src/webapp/main.bc.js webapp/www/js/main.bc.js
endif

# drop the db if it exists, and re-create it
init-db:
	killall ft || echo OK
	killall ton-merge-freeton || echo OK
	psql -tc "SELECT 1 FROM pg_database WHERE datname = '$(DATABASE)'" | grep -q 1 && dropdb $(DATABASE) || echo No database
	psql -tc "SELECT 1 FROM pg_database WHERE datname = '$(DATABASE)'" | grep -q 1 || createdb $(DATABASE)

create-db: init-db

# If you don't have spice, and you didn't modify the contracts, everything
# there should be up-to-date, so let's just keep the old files
no-spice: 
	touch $(SPICED_CONTRACTS) $(SPICED_CODES)

no-liquidity:
	touch $(TZ_CONTRACTS) $(LOVE_CONTRACTS)

no-ft:
	touch contracts/free-ton/*.code

psql:
	psql $(DATABASE)

db-updater: submodule
	PGDATABASE=$(DATABASE) dune build src/db/migrations

db-update: db-updater
	@echo "Migrating Database ..."
	_build/default/src/db/migrations/db_updater.exe --database $(DATABASE) --allow-downgrade

db-downgrade: db-updater
	_build/default/src/db/migrations/db_updater.exe --allow-downgrade --database $(DATABASE) --target `expr $(DBVERSION) - 1` # --use-current

openapi: _build/doc/openapi.json
_build/doc/openapi.json: _build/default/src/api/doc/openapi.exe
	@mkdir -p _build/doc/
	_build/default/src/api/doc/openapi.exe --version "1.0 revision $$(git describe --exclude v* --always)" --title "TON merger API" --contact "contactorigin-labs.com" --servers "api" $(API_HOST) -o _build/doc/openapi.json

update-doc: openapi
	mkdir -p doc
	cp -r _build/doc/openapi.json doc/openapi.json

view-doc: update-doc
	xdg-open 'http://localhost:28881' & redoc-cli serve -p 28881 -w doc/openapi.json

ifeq (${HAS_LIQUIDITY},false)
dune-contracts:
	@echo HAS_LIQUIDITY=false
else
dune-contracts: $(TZ_CONTRACTS) $(LOVE_CONTRACTS)
endif

ifeq (${HAS_SPICE},false)
spiced-contracts:
	@echo HAS_SPICE=false
else
spiced-contracts: $(SPICED_CONTRACTS) $(SPICED_CODES)
endif

$(DUNE_TZ_CONTRACTS_DIR)/%.tz: $(DUNE_CONTRACTS_DIR)/%.liq
	@mkdir -p $(DUNE_TZ_CONTRACTS_DIR)
	@echo -n \\e[1m$(<F)\\e[0m && \
	$(LIQUIDITY) $< -o $@ 2>/tmp/tmmliqout && \
	echo " \\t\\e[92mOK\\e[0m" || echo \\e[91m ; \
	cat /tmp/tmmliqout; echo -n \\e[0m

$(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json: $(DUNE_CONTRACTS_DIR)/%.liq
	@mkdir -p $(DUNE_LOVE_CONTRACTS_DIR)
	@echo -n \\e[1m$(<F)\\e[0m && \
	$(LIQUIDITY) -t Love --json --no-annot $< -o $@ 2>/tmp/tmmliqout && \
	echo " \\t\\e[92mOK\\e[0m" || echo \\e[91m ; \
	cat /tmp/tmmliqout; echo -n \\e[0m

$(CONTRACTS_DEST)/contract_%.ml $(CONTRACTS_DEST)/%_code.ml: $(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json
	cd spice && spice build_ocaml --nickname $*
	cp -f spice/generated_code/ocaml/$*.0/contract_$*.ml $(CONTRACTS_DEST)/
	cp -f spice/generated_code/ocaml/$*.0/$*_code.ml $(CONTRACTS_DEST)/
	# headache -h scripts/header -c scripts/headache_config \
	# 	$(CONTRACTS_DEST)/contract_$*.ml \
	# 	$(CONTRACTS_DEST)/$*_code.ml

submodule: libs/dune-metal/SUBMODULE

libs/dune-metal/SUBMODULE:
	git submodule init
	git submodule update
	touch libs/dune-metal/SUBMODULE

_opam:
	opam repo add ocp git+https://github.com/OCamlPro/ocp-opam-repository --set-default
	opam switch create . --empty --no-install
	eval $$(opam env)

build-deps: _opam
#	opam depext .b
	opam install . --deps-only --working-dir -y

build-dev-deps: build-deps
	opam install -y merlin ocp-indent

clean:
	dune clean
