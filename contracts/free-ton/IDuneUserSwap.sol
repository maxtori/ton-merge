/* Interface of a User Swap Contract */

pragma ton-solidity >= 0.37.0;

interface IDuneUserSwap {

  function receiveCredit() external ;
  function creditDenied() external ;
  
  function updateAfterChange( uint256[] relays,
                              uint8[] indexes,
                              uint8 nreqs,
                              uint64 merge_expiration_date
                              ) external;
}
