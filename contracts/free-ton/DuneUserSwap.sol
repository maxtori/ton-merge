pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneUserSwap.sol";
import "./IDePool.sol";
import "./IParticipant.sol";
import "./IDuneRootSwap.sol";

import "./DuneRelayBase.sol";
import "./DuneConstBase.sol";
import "./DuneHashBase.sol";

/// @title Swap Order Contract
/// @author OCamlPro (https://www.ocamlpro.com)
/* An instance of this contract is created for every swap order issued
   on ths other blockchain. The instance is created by the
   DuneRootSwap contract, on a message sent by any relay. At least
   nreqs relays have to confirmed it before it can ask credits from
   the DuneGiver contract.
*/

contract DuneUserSwap is
  IDuneUserSwap,  // interface
  IParticipant,   // interface to interact with DePool
  DuneRelayBase,  // relays management
  DuneConstBase,  // common constants
  DuneHashBase    // swap hash computation
{


  /*********************************************************************/

  //                          VARIABLES

  /*********************************************************************/

  
  // static variables, unique to a given instance of a contract
  address static s_root_address ;   // address of DuneRootSwap
  uint256 static s_swap_hash ;      // hash of the swap

  // instance variables
  uint64 public g_merge_expiration_date ; // nothing can happen after this
  uint64 public g_swap_expiration_time ;  // time before expiration of a swap

  bool g_initialized = false ;

  // these variables are not set unless g_initialized is true
  string g_order_id ;
  uint256 g_hashed_secret ;
  uint64 g_ton_amount ;
  address g_dest ;
  address g_depool ; // can be address(0) if not vested
  uint8 g_state ;    // initial state is STATE_WAITING_FOR_CONFIRMATION
  uint32 g_state_count ; // increased at every state modification
  uint8 g_nconfirms ;    // number of relays that confirmed the swap
  uint64 g_voteMask ;    // bitmap of the relays that confirmed the swap
  uint64 g_swap_expiration_date ;  // expired after this date
  uint8 g_failed_revelations ;  // limit the number of allowed revelations

  uint8 g_deployer_index ; // index of the relay that announced this swap
  bool public g_testing  ;  // whether we are in testing mode



  /*********************************************************************/

  //                          MODIFIERS

  /*********************************************************************/


  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  modifier AuthOwnerOrRelay() {
    if ( msg.pubkey() != tvm.pubkey() ) _isRelay( msg.pubkey() );
    _;
  }

  modifier AuthRoot() {
    require( msg.sender == s_root_address, EXN_AUTH_FAILED );
    _;
  }



  /*********************************************************************/

  //                          CONSTRUCTOR 

  /*********************************************************************/


  /// @dev constructor called by DuneRootSwap
  /// @param deployer_index Index of the first relay that announced this swap,
  ///   coming first to recover it in onBounce
  /// @param merge_expiration_date The date when the merge is finished, nothing
  ///    should work after this time
  /// @param nreqs Number of required confirmations by relays
  /// @param relays Pubkeys of relays
  /// @param indexes Indexes of relays
  /// @param swap_expiration_time Duration of a swap
  constructor(uint8 deployer_index,
              uint64 merge_expiration_date,
              uint8 nreqs,
              uint256[] relays,
              uint8[] indexes,
              uint64 swap_expiration_time,
              bool testing
              ) public AuthRoot()
    {
      tvm.accept();

      // We don't check nreqs and relays, assuming they are correct by
      // construction. Anyway, if false, we are in a bad situation
      g_nreqs = nreqs ;
      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        g_relays[ relays[i] ] = indexes[i] ;
      }
      g_merge_expiration_date = merge_expiration_date;
      g_swap_expiration_time = swap_expiration_time ;
      g_state = STATE_WAITING_FOR_CONFIRMATION ;
      g_deployer_index = deployer_index ;
      g_testing = testing ;
      
      // Announce the creation of the contract to the relays
      IDuneRootSwap( s_root_address )
        .userSwapDeployed{ value: ROOT_MSG_FEE }
      ( tvm.pubkey(), s_swap_hash, address(this), deployer_index );
    }



  /*********************************************************************/

  //                          PRIVATE FUNCTIONS

  /*********************************************************************/

  /// @dev Returns the index of the relay associated with the pubkey or fails
  function _isRelay( uint256 pubkey ) private view returns ( uint8 index ) {
    index = _isRelayExn(pubkey, EXN_AUTH_FAILED);
  }

  /// @dev Ask the DuneRootSwap to credit the contract and change the
  /// state to STATE_WAITING_FOR_CREDIT
  function _requestCredit() private
  {
    if( address(this).balance > 0.2 ton ){
      IDuneRootSwap( s_root_address ).
        creditOrder{ value: ROOT_MSG_FEE }
      ( g_order_id,
        s_swap_hash,
        g_ton_amount,
        tvm.pubkey() );
      
      _orderStateChanged( STATE_WAITING_FOR_CREDIT ) ;
    }
  }

  /// @dev Transfer the tokens to the account and change the state to
  /// STATE_TRANSFERRED, if not vested.  Otherwise, send the tokens to
  /// the DePool and change the state to STATE_WAITING_FOR_DEPOOL
  function _maybeTransferOrder() private {
    
    if ( g_depool == address(0) ){

      _orderStateChanged( STATE_TRANSFERRED ) ;
      g_dest.transfer({ value: g_ton_amount, bounce: false, flag: 1 });

      } else {

      _orderStateChanged( STATE_WAITING_FOR_DEPOOL ) ;
      IDePool( g_depool ).
        addVestingStake { flag: 1, value: g_ton_amount + DEPOOL_FEE }
      ( g_ton_amount,
        g_dest,
        uint32( VESTING_INCREMENT ),
        uint32( VESTING_PERIOD ) );
    }
  }

  /// @dev change the state of the contract, informing the relays of
  /// the change
  function _orderStateChanged( uint8 state ) private
  {
    g_state = state ;
    g_state_count ++;

    IDuneRootSwap( s_root_address )
      .orderStateChanged
      { value: ROOT_MSG_FEE }
    (
     tvm.pubkey(), s_swap_hash, g_order_id,
     g_state_count, state );
  }

  
  /*********************************************************************/

  //                          PUBLIC FUNCTIONS

  /*********************************************************************/


  /// @dev Confirm a given order by a relay. If it's the first
  /// confirmation, enter the information about the swap. If g_nreqs
  /// relays confirm the order, change the state to
  /// STATE_FULLY_CONFIRMED and ask the root contract to credit this contract
  /// so that the user may reveal his secret
  /// @param order_id Unique identifier of the swap order
  /// @param hashed_secret Secret hashed with sha256
  /// @param ton_amount Amount of nanotons to transfer
  /// @param dest Target address of the final transfer
  /// @param depool If not 0, address of a DePool contract for vesting
  /// @param swap_date Date of the swap creation
  function confirmOrder(
                         string order_id,
                         uint256 hashed_secret ,
                         uint64 ton_amount,
                         address dest,
                         address depool,
                         uint64 swap_date
                         ) public
  {
    uint8 index = _isRelay( msg.pubkey () );
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    require( g_state == STATE_WAITING_FOR_CONFIRMATION, EXN_ALREADY_CONFIRMED );
    tvm.accept();

    // TODO: we should probably see a failure here as a malicious
    // relay, and warn other relays about it ?
    require( _getSwapHash( tvm.pubkey(),
                           order_id, hashed_secret, ton_amount,
                           dest, depool, swap_date ) == s_swap_hash,
             EXN_WRONG_SWAP_HASH );

    if( ! g_initialized ){
      g_initialized = true;

      g_order_id = order_id ;
      g_hashed_secret = hashed_secret ;
      g_ton_amount = ton_amount ;
      g_dest = dest ;
      g_depool = depool ;
      g_swap_expiration_date = swap_date + g_swap_expiration_time;

      g_state_count = 0 ;
      g_nconfirms = 0 ;
      g_voteMask = 0 ;
      g_failed_revelations = 0 ;
    }

    // prevent confirmation after expiration date
    require( uint64(now) < g_swap_expiration_date, EXN_SWAP_EXPIRED );

    uint64 bit = uint64(1) << index ;
    require(  ( g_voteMask & bit ) == 0 , EXN_ALREADY_CONFIRMED );
    g_voteMask |= bit ;
    g_nconfirms++ ;

    IDuneRootSwap( s_root_address )
      .orderConfirmedByRelay{ value: ROOT_MSG_FEE }
    ( tvm.pubkey(), s_swap_hash,
      order_id, msg.pubkey() );

    if( g_nconfirms >= g_nreqs &&
        g_state == STATE_WAITING_FOR_CONFIRMATION ) {
      _orderStateChanged( STATE_FULLY_CONFIRMED );
      IDuneRootSwap( s_root_address )
        .deployedConfirmed{ value: ROOT_MSG_FEE }
      ( tvm.pubkey(), s_swap_hash, g_deployer_index );

      _requestCredit();
    }
  }


  function requestCredit() public
    AuthOwnerOrRelay()
  {
    require( g_state == STATE_CREDIT_DENIED, EXN_UNKNOWN_ORDER );
    require( uint64(now) < g_swap_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    _requestCredit();
  }









  function revealOrderSecret( string secret ) public
    AuthOwnerOrRelay()
  {
    require ( g_state == STATE_CREDITED, EXN_NOT_YET_CREDITED );
    require( g_failed_revelations < MAX_FAILED_REVELATIONS,
             EXN_MAX_FAILED_REVELATIONS );
    tvm.accept();

    uint8 revelation_state ;

    if( uint64(now) > g_swap_expiration_date ) {

      revelation_state = REVEL_STATE_EXPIRED ;
      g_failed_revelations = MAX_FAILED_REVELATIONS ;

    } else {

      uint256 computed_hash = sha256( secret.toSlice() );
      if( computed_hash == g_hashed_secret  ){

        revelation_state = REVEL_STATE_SUCCESS ;
        _orderStateChanged( STATE_REVEALED ) ;

        uint128 balance = address(this).balance;
        if( balance > g_ton_amount + DEPOOL_FEE +
            STANDARD_MSG_FEE + ROOT_MSG_FEE ){

          _maybeTransferOrder();
        }

      } else {

        revelation_state = REVEL_STATE_FAILED ;
        g_failed_revelations++ ;

      }


    }

    IDuneRootSwap( s_root_address )
      .orderSecretRevealed{ value: ROOT_MSG_FEE }
    ( tvm.pubkey(),
      s_swap_hash,
      g_order_id,
      secret,
      revelation_state );

  }




  function transferOrder() public AuthOwnerOrRelay()
  {
    require( g_state == STATE_REVEALED ||
             g_state == STATE_DEPOOL_DENIED, EXN_NOT_YET_REVEALED );
    tvm.accept();
    uint128 balance = address(this).balance;
    require( balance > g_ton_amount + DEPOOL_FEE +
             STANDARD_MSG_FEE + ROOT_MSG_FEE, EXN_BALANCE_TOO_LOW );
    _maybeTransferOrder();
  }

  function cancelOrder() public AuthOwnerOrRelay()
  {
    require( g_state != STATE_WAITING_FOR_CREDIT 
             && g_state != STATE_WAITING_FOR_DEPOOL
             && g_state != STATE_REVEALED
             && g_state != STATE_DEPOOL_DENIED
             // since orders are deleted, no need for:
             //   && g_state != STATE_TRANSFERRED
             //   && g_state != STATE_CANCELLED
             , EXN_NOT_CANCELLABLE );
    require( uint64(now) > g_swap_expiration_date, EXN_NOT_YET_EXPIRED );

    if( g_state == STATE_CREDITED ){
      require( address(this).balance > g_ton_amount, EXN_BALANCE_TOO_LOW );
      tvm.accept();
      s_root_address.transfer({ value: g_ton_amount, bounce: false, flag: 0 });
    } else {
      tvm.accept();
    }
    _orderStateChanged( STATE_CANCELLED ) ;
  }



  function receiveCredit() public override AuthRoot()
  {
    _orderStateChanged( STATE_CREDITED ) ;
  }






  function creditDenied() public override AuthRoot()
  {
    tvm.accept();
    _orderStateChanged( STATE_CREDIT_DENIED ) ; 
  }






  function closeSwap() public AuthOwnerOrRelay()
  {
    require( g_testing || uint64(now) > g_merge_expiration_date,
             EXN_SWAP_NOT_EXPIRED );
    tvm.accept();
    selfdestruct( s_root_address );
  }




  function receiveAnswer(uint32 errcode,
                         uint64 //comment
                         ) public override
  {
    require( msg.sender == g_depool , EXN_AUTH_FAILED );
    require( g_state == STATE_WAITING_FOR_DEPOOL,
             EXN_UNEXPECTED_ANSWER_FROM_DEPOOL );

    if ( errcode == 0 ){ // SUCCESS
      _orderStateChanged( STATE_TRANSFERRED ) ;
    } else {
      _orderStateChanged( STATE_DEPOOL_DENIED ) ;
    }
  }




  function updateAfterChange( uint256[] relays,
                              uint8[] indexes,
                              uint8 nreqs,
                              uint64 merge_expiration_date
                              ) public override AuthRoot()
  {
    mapping ( uint256 => uint8 ) map ;
    uint256 len = relays.length;
    for ( uint8 i = 0 ; i < len ; i++ ){
      map[ relays[i] ] = indexes[i] ;
    }
    g_relays = map;
    g_nreqs = nreqs;
    if( merge_expiration_date != 0 ){
      g_merge_expiration_date = merge_expiration_date ;
    }
  }

  /*********************************************************************/

  //                          SYSTEM

  /*********************************************************************/


  fallback () external {}
  receive () external {}  // same as default

  
  /*********************************************************************/

  //                          GETTERS

  /*********************************************************************/


  function get() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes,
     address root_address,
     uint64 merge_expiration_date,
     uint256 swap_hash,
     string order_id,
     uint256 hashed_secret ,
     uint64 ton_amount,
     address dest,
     address depool,
     uint64 swap_expiration_date,
     uint256 pubkey,
     bool testing
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
    root_address = s_root_address ;
    swap_hash = s_swap_hash ;

    merge_expiration_date = g_merge_expiration_date ;

    order_id = g_order_id ;
    hashed_secret = g_hashed_secret ;
    ton_amount = g_ton_amount ;
    dest = g_dest ;
    depool = g_depool ;
    swap_expiration_date = g_swap_expiration_date ;
    pubkey = tvm.pubkey();
    testing = g_testing ;
  }

  function getOrderRemainingTime()
    public view returns ( uint64 remaining_time )
  {
    uint64 now64 = uint64(now);
    if( now64 > g_swap_expiration_date )
      remaining_time = 0 ;
    else
      remaining_time = g_swap_expiration_date - now64 ;
  }


}

