pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

abstract contract DuneHashBase {

  function _getSwapHash(
                        uint256 pubkey,
                        string order_id,
                        uint256 hashed_secret ,
                        uint64 ton_amount,
                        address dest,
                        address depool,
                        uint64 swap_date
                        ) internal pure returns ( uint256 swap_hash )
  {
    TvmBuilder builder1 ;
    builder1.store( pubkey,               // 32 bytes
                    order_id,             // 10 bytes
                    hashed_secret,        // 32 bytes
                    ton_amount);           // 8 bytes
    TvmBuilder builder2 ;
    builder2.store(dest,                 // 33 bytes
                   depool,               // 32 bytes
                   swap_date             // 8 bytes
                   );
    TvmBuilder builder ;
    builder.store(
                   tvm.hash( builder1.toCell () ),
                   tvm.hash( builder2.toCell () ) );
    swap_hash = tvm.hash( builder.toCell() );
  }

  function getSwapHash(
                       uint256 pubkey,
                       string order_id,
                       uint256 hashed_secret ,
                       uint64 ton_amount,
                       address dest,
                       address depool,
                       uint64 swap_date
                       ) public pure returns ( uint256 swap_hash )
  {
    swap_hash = _getSwapHash( pubkey,
                              order_id,
                              hashed_secret,
                              ton_amount,
                              dest,
                              depool,
                              swap_date );
  }

  function getTime() public pure returns (uint64 time)
  {
    time = now;
  }
  
}
