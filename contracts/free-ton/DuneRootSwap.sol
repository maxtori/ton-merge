/*
  The goal of this contract is to safeguard the list of relays/oracles that
  are allowed to sign transactions on UserSwaps.
 */

// TODO: add a budget/gas section in the architecture

pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./DuneUserSwap.sol";
import "./IDuneRootSwap.sol";
import "./DuneRelayBase.sol";
import "./DuneConstBase.sol";





contract DuneRootSwap is IDuneRootSwap,
  DuneConstBase, // all constants like EXN_ or STATE_
  DuneRelayBase, // identification of relays
  DuneHashBase   // computation of swap hashes
{


  /*********************************************************************/

  //                          TYPES and EVENTS

  /*********************************************************************/

  // a Change of configuration: add or remove relays, change number of
  // confirmations required
  struct Change {
    uint64 id ;
    uint64 creation_time ;
    uint64 ackMask ;
    uint64 rejMask ;
    uint8 n_ack ;
    uint8 n_rej ;

    uint8 nreqs ;
    uint256[] add_relays ;
    uint256[] del_relays ;
    uint64 merge_expiration_date ;
  }

  // emitted when a relay sends a ping to display its status
  event RelayPingPong( uint256 pubkey, uint8 kind, uint8 version );

  // emitted when an attempt at deploying a contract is made
  event DeployUserSwap(uint256 pubkey, uint256 swap_hash, address user_addr);

  // emitted when a contract was successfully deployed
  event UserSwapDeployed(uint256 pubkey, uint256 swap_hash, address user_addr);

  // emitted when a relay just confirmed a swap
  event OrderConfirmedByRelay ( string order_id , uint256 pubkey ); 

  // emitted when we decide to credit or not a swap
  event CreditOrder( string order_id, bool accepted, uint128 ton_amount );

  // emitted when the state of a swap changes
  event OrderStateChanged(string order_id, uint32 state_count, uint8 state);

  // emitted when the secret of a swap is successfully revealed
  event OrderSecretRevealed( string order_id, string secret, uint8 status );

  /*********************************************************************/

  //                          MODIFIERS

  /*********************************************************************/

  // external message sent by the admin or a relay
  modifier AuthOwnerOrRelay() {
    if ( msg.pubkey() != tvm.pubkey() ) _isRelay( msg.pubkey() );
    _;
  }

  // external message sent by the admin
  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  // internal message sent by a DuneUserSwap
  modifier AuthUser(uint256 pubkey, uint256 swap_hash) {
    address user_addr = _getUserSwapAddress( pubkey, swap_hash );
    require( msg.sender == user_addr, EXN_AUTH_FAILED );
    _;
  }

  modifier OwnerSet() {
    require( tvm.pubkey() != 0, EXN_MISSING_PUBKEY );
    _;
  }

  modifier RootSet() {
    require( g_root_address != address(0), EXN_NOT_INITIALIZED );
    _;
  }



  /*********************************************************************/

  //                          VARIABLES

  /*********************************************************************/

  uint8 public g_nrelays ;       // number of relays
  uint8 public g_relay_counter ;

  uint64 g_change_counter ;
  mapping(uint64 => Change) g_changes;

  // keep the last deployed swapped in memory to prevent redeploying it again
  // immediately. not really useful since we handle bounced messages
  uint256 public g_prev_user_key ;
  uint256 public g_prev_swap_hash ;

  // duration of a swap before being cancelled if no revelation
  uint64 g_swap_expiration_time ;

  // number of unconfirmed deployments launched by a given relay. There is a
  // maximum to prevent a malicious relay from flooding with them
  mapping(uint8 => uint32) public g_unconfirmed_deployments ;

  // address of the main FreeTON giver for refund after the merge
  address public g_freeton_giver ;

  // address of the DuneRootSwap contract
  address public g_root_address ;

  // code of DuneUserSwap contract to compute and verify addresses
  TvmCell public g_duneUserSwapCode ;

  // date of expiration for all swaps (one week after Dune's expiration date)
  uint64 public g_merge_expiration_date ;

  // whether we are in testing mode
  bool public g_testing  ;

  /*********************************************************************/

  //                          CONSTRUCTOR 

  /*********************************************************************/


  /// @param relays The initial set of relays/oracles public keys
  /// @param nreqs The initial minimal number of confirmation for a swap order
  /// @param duneUserSwapCode The code of the DuneUserSwap contract to be able
  ///        to deploy user contracts
  /// @param merge_expiration_date The date after which no new user contract
  ///        can be created
  constructor(
              uint256[] relays,
              uint8 nreqs,
              TvmCell duneUserSwapCode,
              uint64 merge_expiration_date,
              uint64 swap_expiration_time,
              bool testing
              ) OwnerSet() AuthOwner() public
    {
      require( relays.length > 0 && relays.length <= MAX_RELAYS,
               EXN_BAD_NUMBER_OF_RELAYS );
      tvm.accept();

      g_swap_expiration_time = swap_expiration_time ;
      g_merge_expiration_date = merge_expiration_date + swap_expiration_time ;

      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _addRelay( relays [i] );
      }
      require( nreqs > 0 && nreqs <= g_nrelays,
               EXN_BAD_NUMBER_OF_CONFIRMATIONS );
      g_nreqs = nreqs ;

      g_duneUserSwapCode = duneUserSwapCode ;
      g_root_address = address( this );
      g_testing = testing ;
    }




  /*********************************************************************/

  //                          PUBLIC FUNCTIONS: for ADMIN

  /*********************************************************************/

  /// @dev Send back unused tokens to the main giver when expiration
  /// date is passed.
  /// Caller is admin with same pubkey. Since this contract MUST exist until
  /// all other contracts have been closed, only the admin should be able
  /// close it.
  function closeSwap() public AuthOwner() {
    require( g_testing || uint64(now) > g_merge_expiration_date ,
             EXN_SWAP_NOT_EXPIRED ) ;
    tvm.accept();
    selfdestruct( g_freeton_giver );
  }

  /*********************************************************************/

  //                          PRIVATE FUNCTIONS: swaps

  /*********************************************************************/

  // compute the address of the DuneUserSwap contract associated with
  // a given swap
  function _getUserSwapAddress( uint256 pubkey, uint256 swap_hash )
    internal view returns ( address addr )
  {
    /* Recompute the address that a user with the provided pubkey
       should have, if he uses the same contract and static
       variables. */
    TvmCell stateInit = tvm.buildStateInit({
      contr: DuneUserSwap,
          pubkey: pubkey,
          code: g_duneUserSwapCode,
          varInit: {
             s_root_address: g_root_address ,
             s_swap_hash : swap_hash
            }
          });
    addr = address(tvm.hash(stateInit));
  }

  // returns the current number of unconfirmed deployments for a given relay
  function _getUnconfirmedDeployments( uint8 relay_index ) private view
    returns ( uint32 count )
  {
    optional(uint32) opt_count = g_unconfirmed_deployments.fetch( relay_index );
    if( opt_count.hasValue() ){
      count = opt_count.get();
    } else {
      count = 0 ;
    }
  }

  // decrease the number of unconfirmed deployments when a swap is confirmed
  function _deployedConfirmed( uint8 relay_index ) private
  {
    uint32 count = _getUnconfirmedDeployments( relay_index );
    if( count > 0 ){ // how could it be different ?
      g_unconfirmed_deployments [ relay_index ] = count - 1;
    }
  }

  // returns the required number of confirmations, the pubkeys of relays
  // and their indexes
  function _getState() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
  }
  
  /*********************************************************************/
  
  //                          PUBLIC FUNCTIONS for RELAYS: swaps
  
  /*********************************************************************/
  
  
  /// @dev deploy the contract associated with a swap order.
  /// @param pubkey User pubkey for the swap
  /// @param swap_hash Hash of the swap
  function deployUserSwap( uint256 pubkey, uint256 swap_hash ) public
    returns ( address newSwap )
  {
    require( g_prev_user_key != pubkey ||
             g_prev_swap_hash != swap_hash,
             EXN_ALREADY_DEPLOYED );
    uint8 relay_index = _isRelay( msg.pubkey() ) ;
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    uint32 count = _getUnconfirmedDeployments( relay_index );
    require( count < MAX_UNCONFIRMED_DEPLOYMENTS,
             EXN_MAX_UNCONFIRMED_DEPLOYMENTS) ;
    (uint8 nreqs, uint256[] relays, uint8[] indexes) = _getState();

    newSwap = new DuneUserSwap
      {
      code: g_duneUserSwapCode ,
      value: 1 ton ,
      pubkey: pubkey ,
      varInit: {
                s_root_address: g_root_address,
                s_swap_hash: swap_hash
      }
      }( relay_index,
         g_merge_expiration_date, nreqs, relays, indexes,
         g_swap_expiration_time, g_testing ) ;

    g_prev_user_key = pubkey ;
    g_prev_swap_hash = swap_hash ;
    emit DeployUserSwap( pubkey, swap_hash, newSwap );
  }

  function relayPingPong( uint8 kind, uint8 version ) public view
  {
    uint256 pubkey = msg.pubkey() ;
    _isRelay( pubkey );
    tvm.accept();
    emit RelayPingPong( pubkey, kind, version );
  }


  /*********************************************************************/

  //                          PUBLIC FUNCTIONS: for DuneUserSwap

  /*********************************************************************/

  function deployedConfirmed( uint256 pubkey, uint256 swap_hash,
                              uint8 relay_index )
    public override AuthUser(pubkey, swap_hash)
  {
    tvm.accept();
    _deployedConfirmed( relay_index );
  }


  // @dev Transfer TON tokens to a DuneUserSwap contract
  // @param credit_id Unique identifier of the swap
  // @param ton_amount Amount of nanoton to transfer back
  // @param pubkey Public key associated with the swap user
  function creditOrder(
                         string order_id,
                         uint256 swap_hash,
                         uint128 ton_amount,
                         uint256 pubkey) public override RootSet()
  {
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    bool credited = false ;
    
    address computed_addr = _getUserSwapAddress( pubkey, swap_hash );

    // Fail if the caller is not the DuneUserSwap associated with this
    // pubkey.
    require( msg.sender == computed_addr, EXN_AUTH_FAILED );

    if( address(this).balance >= ton_amount ){

      // Otherwise, send back the requested tokens
      // TODO: it may fail if we don't have enough balance
      IDuneUserSwap( computed_addr ).receiveCredit
        { value: ton_amount + ( 1 ton ), bounce: true, flag: 0 }
      ();

      credited = true ;
    } else {
      IDuneUserSwap( msg.sender ).creditDenied();
    }
    emit CreditOrder ( order_id, credited, ton_amount );
  }


  function userSwapDeployed ( uint256 pubkey,
                              uint256 swap_hash,
                              address user_addr,
                              uint8 relay_index
                              ) public override AuthUser(pubkey, swap_hash)
  {
    tvm.accept();
    uint32 count = _getUnconfirmedDeployments( relay_index );
    g_unconfirmed_deployments [ relay_index ] = count + 1;
    emit UserSwapDeployed( pubkey, swap_hash, user_addr );
  }

  function orderStateChanged( uint256 pubkey,
                              uint256 swap_hash,
                              string order_id,
                              uint32 state_count,
                              uint8 state
                              ) public override AuthUser(pubkey, swap_hash)
  {
    tvm.accept();
    emit OrderStateChanged( order_id , state_count, state );
  }

  function orderSecretRevealed( uint256 pubkey,
                                uint256 swap_hash,
                                string order_id,
                                string secret,
                                uint8 status
                                ) public override AuthUser(pubkey, swap_hash)
  {
    tvm.accept();
    emit OrderSecretRevealed( order_id, secret, status );
  }

  function orderConfirmedByRelay ( uint256 pubkey,
                                   uint256 swap_hash,
                                   string order_id,
                                   uint256 relay_pubkey
                                   ) public override AuthUser(pubkey, swap_hash)
  {
    emit OrderConfirmedByRelay( order_id , relay_pubkey );
  }

  
  /*********************************************************************/

  //                          PUBLIC FUNCTIONS for RELAYS: changes

  /*********************************************************************/

  
  function cleanChanges( ) public {
    _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( g_change_counter );
    require( ! opt_change.hasValue() , EXN_CHANGE_NOT_EXPIRED );
  }




  function proposeChange( uint8 nreqs,
                          uint256[] add_relays,
                          uint256[] del_relays,
                          uint64 merge_expiration_date
                          )
    public returns ( uint64 changeId ) {
    uint8 index = _isRelay( msg.pubkey() ) ;
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    optional(Change) opt_prev = _getChange( g_change_counter );
    require ( !opt_prev.hasValue() , EXN_CHANGE_ALREADY_PROPOSED );

    g_change_counter++ ;
    changeId = g_change_counter ;
    Change change = Change(
                           changeId,
                           now, // creation_time
                           0, // ackMask
                           0, // rejMask
                           0, // n_ack
                           0, // n_rej
                           nreqs,
                           add_relays,
                           del_relays,
                           merge_expiration_date
                           );
    g_changes[ changeId ] = change ;

    _acceptChange( change, index );
  }



  function acceptChange( uint64 changeId ) public {
    require( changeId > 0 && changeId == g_change_counter,
             EXN_WRONG_CHANGE_ID );
    uint8 index = _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( changeId );
    _acceptChange( opt_change.get(), index );
  }




  function rejectChange( uint64 changeId ) public {
    require( changeId > 0 && changeId == g_change_counter,
             EXN_WRONG_CHANGE_ID );
    uint8 index = _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( changeId );
    _rejectChange( opt_change.get(), index );
  }



  /*********************************************************************/

  //                          PRIVATE FUNCTIONS: changes

  /*********************************************************************/



  function _addRelay( uint256 key ) private {

    if( ! g_relays.exists(key) ){
      require( g_relay_counter < MAX_RELAYS, EXN_TOO_MANY_RELAYS );
      g_relays[key] = g_relay_counter++ ;
      g_nrelays++ ;
    }

  }

  function _delRelay( uint256 key ) private {

    if( g_relays.exists(key) ){
      delete g_relays[key];
      g_nrelays-- ;
    }
  }

  function _isRelay( uint256 key ) private view returns ( uint8 index ) {
    index = _isRelayExn(key, EXN_NO_RELAY_FOR_PUBKEY);
  }



  function _getChange( uint64 changeId ) private
    returns ( optional(Change) opt_change ){

    opt_change = g_changes.fetch ( changeId );
    if ( opt_change.hasValue() ){
      Change change = opt_change.get() ;
      if( uint64(now) - change.creation_time > CHANGE_EXPIRATION_TIME ){
        opt_change.reset();
        delete g_changes[ changeId ];
      }
    }
  }



  function _acceptChange( Change change, uint8 index ) private {

    uint64 bit = uint64(1) << index ;
    require ( ( change.ackMask & bit ) == 0, EXN_ALREADY_VOTED_FOR_CHANGE );
    change.ackMask |= bit ;
    change.n_ack++;

    if( change.n_ack >= g_nreqs ){
      delete g_changes[ change.id ];
      g_nreqs = change.nreqs ;

      uint256[] relays = change.del_relays;
      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _delRelay( relays [i] );
      }
      relays = change.add_relays;
      len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _addRelay( relays [i] );
      }
      require ( g_nrelays > 0 , EXN_BAD_NUMBER_OF_RELAYS );
      require ( g_nreqs > 0 && g_nreqs <= g_nrelays,
                EXN_BAD_NUMBER_OF_CONFIRMATIONS );

      delete g_changes[ change.id ];
      if( change.merge_expiration_date != 0 ){
        g_merge_expiration_date = change.merge_expiration_date;
      }
    } else {
      g_changes [ change.id ] = change ;
    }
  }



  function _rejectChange( Change change, uint8 index ) private {

    uint64 bit = uint64(1) << index ;
    require ( ( change.rejMask & bit ) == 0, EXN_ALREADY_VOTED_FOR_CHANGE );
    change.rejMask |= bit ;
    change.n_rej++;

    if( change.n_rej >= g_nreqs ){
      delete g_changes[ change.id ];
    } else {
      g_changes [ change.id ] = change ;
    }
  }

  function updateRelays( uint256 user_pubkey, uint256 swap_hash )
    public view AuthOwnerOrRelay() {
    tvm.accept();

    address computed_addr = _getUserSwapAddress( user_pubkey, swap_hash );

    (uint8 nreqs, uint256[] relays, uint8[] indexes) = _getState();
    IDuneUserSwap( computed_addr ). updateAfterChange
      ( relays, indexes, nreqs, g_merge_expiration_date );
  }




  /*********************************************************************/

  //                          GETTERS

  /*********************************************************************/


  function getChange() public view returns (Change change)
  {
    optional(Change) opt_change = g_changes.fetch ( g_change_counter );
    change = opt_change.get();
  }



  function getUserSwapAddress( uint256 pubkey, uint256 swap_hash  )
    public view returns ( address addr )
  {
    addr = _getUserSwapAddress( pubkey, swap_hash );
  }

  function remainingTime() public view returns (uint64 remaining_time)
  {
    uint64 now64 = uint64(now);
    if(now64 > g_merge_expiration_date){
      remaining_time = 0;
    } else {
      remaining_time = g_merge_expiration_date - now64 ;
    }
  }



  function getConfig() public view returns
    ( uint64 merge_expiration_date, uint64 swap_expiration_time )
  {
    merge_expiration_date = g_merge_expiration_date ;
    swap_expiration_time = g_swap_expiration_time ;
  }

  function get() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes,
     address root_address,
     uint8 nrelays,
     uint8 relay_counter,
     uint64 merge_expiration_date,
     uint64 swap_expiration_time,
     uint64 change_counter,
     uint8 change_state,
     uint256 prev_user_key,
     bool testing
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
    root_address = g_root_address ;

    nrelays = g_nrelays;
    relay_counter = g_relay_counter ;
    merge_expiration_date = g_merge_expiration_date ;
    swap_expiration_time = g_swap_expiration_time ;
    change_counter = g_change_counter ;
    prev_user_key = g_prev_user_key ;

    optional(Change) opt_change = g_changes.fetch ( change_counter );
    if ( opt_change.hasValue() ){
      Change change = opt_change.get() ;
      if( uint64(now) - change.creation_time > CHANGE_EXPIRATION_TIME ){
        change_state = 2 ; // change expired
      } else {
        change_state = 1 ; // change waiting
      }
    } else {
      change_state = 0 ; // no change waiting
    }
    testing = g_testing ;
  }


  /*********************************************************************/

  //                          SYSTEM

  /*********************************************************************/


  fallback () external {}

  // when the message bounced, it means that DuneUserSwap contract was
  // already deployed by another relay
  onBounce(TvmSlice slice) external {
    uint32 functionId = slice.decode(uint32);
    if(functionId == tvm.functionId(DuneUserSwap)){
      uint8 relay_index = slice.decode(uint8);
      _deployedConfirmed( relay_index );
    }
  }

}

