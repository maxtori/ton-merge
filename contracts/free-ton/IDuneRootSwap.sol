/* Interface IRootUserWallet */

pragma ton-solidity >= 0.37.0;

interface IDuneRootSwap {

  function deployedConfirmed( uint256 pubkey, uint256 swap_hash,
                              uint8 relay_index ) external ;

  function userSwapDeployed ( uint256 pubkey,
                              uint256 swap_hash,
                              address addr,
                              uint8 relay_index
                              ) external ;
  
  function orderStateChanged( uint256 pubkey,
                              uint256 swap_hash,
                              string order_id,
                              uint32 state_count,
                              uint8 state
                             ) external ;
  function orderSecretRevealed( uint256 pubkey,
                              uint256 swap_hash,
                                string order_id,
                                string secret,
                                uint8 status
                                ) external ;

  function orderConfirmedByRelay ( uint256 pubkey,
                                   uint256 swap_hash,
                                   string order_id,
                                   uint256 relay_pubkey
                                   ) external ;

  function creditOrder(
                         string credit_id,
                         uint256 swap_hash,
                         uint128 ton_amount,
                         uint256 pubkey
                         ) external ;
  
}
